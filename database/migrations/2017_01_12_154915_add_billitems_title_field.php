<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillitemsTitleField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->string('title')->nullable()->after('product_id');
        });

        foreach (\App\Billitem::all() as $billitem) {
            if(!$billitem->title) {

                $billitem->title = $billitem->product->title;
                $billitem->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->dropColumn('title');
        });
    }
}
