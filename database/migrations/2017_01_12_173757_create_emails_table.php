<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users');

            $table->integer('bill_id')->unsigned()->nullable()->references('id')->on('bills');

            $table->text('to')->nullable(); //TODO to json
            
            $table->string('theme')->nullable();
            $table->text('body')->nullable();

            $table->string('attachment_type', 8)->nullable();
            $table->string('attachment')->nullable();

            $table->timestamp('sended_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
