<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->references('id')->on('users');

            $table->string('filename')->nullable();

            $table->integer('row_last')->unsigned()->nullable();
            $table->integer('rows_total')->unsigned()->nullable();
            $table->boolean('is_complete')->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
}
