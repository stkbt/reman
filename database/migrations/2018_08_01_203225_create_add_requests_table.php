<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_requests', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('search_history_id')->nullable()->references('id')->on('search_histories');

            $table->unsignedInteger('sender_id')->nullable()->references('id')->on('users');
            $table->unsignedInteger('handler_id')->nullable()->references('id')->on('users');

            $table->string('sender_message', 512)->nullable();
            $table->string('handler_message', 512)->nullable();

            $table->timestamp('handled_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_requests');
    }
}
