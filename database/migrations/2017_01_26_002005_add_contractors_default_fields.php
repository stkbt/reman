<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractorsDefaultFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractors', function (Blueprint $table) {
            
            $table->string('default_paymentmethod')->nullable()->after('bank');
            $table->string('default_delivery_conditions')->nullable()->after('default_paymentmethod');
            $table->string('default_notes')->nullable()->after('default_delivery_conditions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractors', function (Blueprint $table) {
            
            $table->dropColumn('default_paymentmethod');
            $table->dropColumn('default_delivery_conditions');
            $table->dropColumn('default_notes');
        });
    }
}
