<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillitemsCountField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->smallInteger('count')->unsigned()->nullable()->after('currency_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->dropColumn('count');
        });
    }
}
