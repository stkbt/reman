<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users');

            $table->string('title')->nullable();
            
            $table->string('country')->nullable();

            $table->string('city')->nullable();

            $table->string('address')->nullable();
            
            $table->string('phone')->nullable();

            $table->string('email')->nullable();

            $table->string('inn', 16)->nullable();
            
            $table->string('kpp', 16)->nullable();

            $table->string('bank')->nullable();

            $table->timestamps();
        });

        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users');
            $table->integer('contractor_id')->unsigned()->nullable()->references('id')->on('contractors');

            $table->boolean('is_draft')->default(1)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
        Schema::dropIfExists('bills');
    }
}
