<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillsServiceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            
            $table->string('number', 24)->after('currency_id')->nullable()->index();
            $table->string('technics', 64)->after('number')->nullable();
            $table->string('paymentmethod')->after('technics')->nullable();
            $table->string('delivery_time', 64)->after('paymentmethod')->nullable();
            $table->boolean('custom_delivery_time')->after('delivery_time')->nullable()->default(0);
            $table->string('delivery_conditions')->after('custom_delivery_time')->nullable();
            $table->string('notes')->after('delivery_conditions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            
            $table->dropColumn('number');
            $table->dropColumn('technics');
            $table->dropColumn('paymentmethod');
            $table->dropColumn('delivery_time');
            $table->dropColumn('notes');
            $table->dropColumn('delivery_conditions');
            $table->dropColumn('custom_delivery_time');
        });
    }
}
