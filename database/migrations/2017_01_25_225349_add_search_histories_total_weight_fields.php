<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSearchHistoriesTotalWeightFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('search_histories', function (Blueprint $table) {
            
            $table->decimal('total', 10, 4)->unsigned()->nullable()->after('brand_id');
            $table->mediumInteger('weight')->unsigned()->nullable()->after('total');

            $table->string('title', 64)->after('user_id')->nullable();
            $table->string('comment')->after('weight')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search_histories', function (Blueprint $table) {
            
            $table->dropColumn('total');
            $table->dropColumn('weight');
            $table->dropColumn('title');
            $table->dropColumn('comment');
        });
    }
}
