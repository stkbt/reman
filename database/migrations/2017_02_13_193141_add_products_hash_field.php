<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsHashField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            
            $table->string('hash')->nullable()->after('brand_id');
        });
        Schema::table('import_products', function (Blueprint $table) {
            
            $table->string('hash')->nullable()->after('brand_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
        Schema::table('import_products', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
    }
}
