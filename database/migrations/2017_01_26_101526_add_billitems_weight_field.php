<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillitemsWeightField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billitems', function (Blueprint $table) {
            $table->mediumInteger('weight')->unsigned()->nullable()->after('delivery_time_max');
        });

        foreach (\App\Billitem::all() as $billitem) {
            if($billitem->product && !$billitem->weight) {

                $billitem->weight = $billitem->product->weight;
                $billitem->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billitems', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
    }
}
