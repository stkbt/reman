<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('import_id')->unsigned()->reference('id')->on('imports');

            $table->string('sku')->nullable()->index();
            $table->string('sku_alternate')->nullable()->index();
            
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();

            $table->decimal('price', 10, 4)->unsigned()->nullable();

            $table->tinyInteger('currency_id')->unsigned()->references('id')->on('currencies');
            
            $table->integer('delivery_time_min')->unsigned()->nullable();
            $table->integer('delivery_time_max')->unsigned()->nullable();

            $table->mediumInteger('weight')->unsigned()->nullable();

            $table->integer('supplier_id')->unsigned()->references('id')->on('suppliers');
            $table->integer('brand_id')->unsigned()->references('id')->on('brands');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_products');
    }
}
