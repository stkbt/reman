<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('sku')->nullable()->index();
            $table->string('sku_alternate')->nullable()->index();
            
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();

            $table->decimal('price', 10, 4)->unsigned()->nullable();

            $table->tinyInteger('currency_id')->unsigned()->references('id')->on('currencies');
            
            $table->integer('delivery_time_min')->unsigned()->nullable();
            $table->integer('delivery_time_max')->unsigned()->nullable();

            $table->mediumInteger('weight')->unsigned()->nullable();

            $table->integer('supplier_id')->unsigned()->references('id')->on('suppliers');
            $table->integer('brand_id')->unsigned()->references('id')->on('brands');


            $table->string('image')->nullable();
            
            
            $table->timestamp('priced_at')->nullable();
            
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
