<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_id')->unsigned()->references('id')->on('bills');
            $table->integer('product_id')->unsigned()->references('id')->on('products');

            $table->decimal('price', 10, 4)->unsigned()->nullable();
            $table->tinyInteger('currency_id')->unsigned()->references('id')->on('currencies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billitems');
    }
}
