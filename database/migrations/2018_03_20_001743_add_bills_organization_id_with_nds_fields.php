<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillsOrganizationIdWithNdsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            //
            $table->unsignedInteger('organization_id')->nullable()->references('id')->on('organizations')->after('currency_id');
            $table->boolean('with_nds')->nullable()->default(1)->after('currency_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            //
            $table->dropColumn(['organization_id', 'with_nds']);
        });
    }
}
