<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillitemsDeliveryFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->integer('delivery_time_min')->unsigned()->nullable()->after('count');
            $table->integer('delivery_time_max')->unsigned()->nullable()->after('delivery_time_min');
        });

        foreach (\App\Billitem::all() as $billitem) {
            if(!$billitem->delivery_time_min) {

                $billitem->delivery_time_min = $billitem->product->delivery_time_min;
                $billitem->delivery_time_max = $billitem->product->delivery_time_max;
                $billitem->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billitems', function (Blueprint $table) {
            
            $table->dropColumn('delivery_time_min');
            $table->dropColumn('delivery_time_max');
        });
    }
}
