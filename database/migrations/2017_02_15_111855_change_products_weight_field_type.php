<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsWeightFieldType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            
            $table->decimal('weight', 10, 2)->change();
        });
        Schema::table('import_products', function (Blueprint $table) {
            
            $table->decimal('weight', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->mediumInteger('weight')->change();
        });
        Schema::table('import_products', function (Blueprint $table) {
            $table->mediumInteger('weight')->change();
        });
    }
}
