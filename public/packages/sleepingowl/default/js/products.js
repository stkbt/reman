$(document).ready(function(){
    
    var panel_actions = $('[data-type="display-actions"].panel-footer');

    panel_actions
    	.width($('#products_wrapper').width()).hide()
    	.find('#createBill')
    		.removeClass('btn-default').addClass('btn-success')
    		.before(
    			$('<span class="selected_wrapper" />')
    				.html('Выбрано <span id="selected"></span> поз.</span>')
			)
    	;
    
    $(document).on('change', 'input[type="checkbox"][name="_id[]"]', function(e){

    	var selected = 0;

    	if(selected = $('tr:not([data-hidden]) input[type="checkbox"][name="_id[]"]:checked').length) {

    		panel_actions
    			.show()
    			.find('#selected').text(selected).end()
    			.width($('#products_wrapper').width())
    			;	
    	}
    	else {

    		panel_actions.hide();
    	}
    });

    $('[data-nulled] input').remove();

    $(document).on('click', '#products tbody tr', function(e){

        if(e.target.nodeName != 'INPUT' && e.target.nodeName != 'A' && e.target.nodeName != 'I' && e.target.nodeName != 'BUTTON') {

            e.preventDefault();

            if($(this).data('nulled')) return;
            if($(this).data('hidden')) return;

            if($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').removeAttr('checked').trigger('change');
            }
            else {
                $(this).find('input[type="checkbox"]').prop('checked', 'checked').trigger('change');
            }
        }

        
    });


    // $(document).on('click', '#products tbody tr', function(e){

    //     if(e.target.nodeName != 'INPUT' && e.target.nodeName != 'A' && e.target.nodeName != 'I' && e.target.nodeName != 'BUTTON') {

    //         e.preventDefault();

    //         if($(this).data('nulled')) return;
    //         if($(this).data('hidden')) return;

    //         if($(this).find('input[type="checkbox"]').is(':checked')) {
    //             $(this).find('input[type="checkbox"]').removeAttr('checked').trigger('change');
    //         }
    //         else {
    //             $(this).find('input[type="checkbox"]').prop('checked', 'checked').trigger('change');
    //         }
    //     }

        
    // });




    $('#products__form_add').on('submit', function(e){

        e.preventDefault();

        var form = e.target;

        $.post(e.target.action, {sku: $(e.target).find('[name="sku"]').val()}, function(data){

            if(data.success == true && data.item) {

                $('<tr />')
                    .attr('role', 'row')
                    .append(
                        $('<td />')
                            .addClass('sorting_1')
                            .append(
                                $('<input type="checkbox" class="adminCheckboxRow" name="_id[]" value="" />')
                                .val(data.item.id)
                            )
                    )
                    .append(
                        $('<td />')
                            .text(data.item.sku)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.sku_alternate)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.title)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.title_en)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.price_formatted)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.currency_title)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.price_rub)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.delivery_time)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.weight)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.brand_title)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.quantity)
                    )
                    .append(
                        $('<td />')
                            .text(data.item.priced_at)
                    )
                    .append(
                        $('<td />')
                    )
                    .appendTo('#products')
            }
            else {

                $('<tr />')
                    .attr('role', 'row')
                    .attr('data-nulled', '1')
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                            .text(data.sku)
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .append(
                        $('<td />')
                    )
                    .appendTo('#products')
            }
        });

    });


    
    $('#productssearch select[name="brand[]"], #productssearch select[name="country[]"]').on('change', function(){

        console.log($(this).attr('name'));

        filterProducts($(this).attr('name').replace('[]', ''), $(this).val());
    });


    $('#productssearch #delivery_time').ionRangeSlider({
        postfix: ' дн',
        grid: true,
        hide_min_max: true,
        // hide_from_to: true,
        onFinish: function(slider) {

            filterProducts('deliverymax', slider.from);
        }
    });


});

var activeFilters = {};

var filterProducts = function(_field, _value) {

    $('#products_wrapper').addClass('_loading');

    activeFilters[_field] = _value;

    console.log(activeFilters);

    $('#products tbody tr').each(function(index, item){

        item = $(item);

        var is_show = true;


        if(activeFilters['brand']) {
            if(activeFilters['brand'].indexOf(item.data('brand').toString()) == -1) {
                
                is_show = false;
            } 
        }

        if(activeFilters['country']) {
            if(activeFilters['country'].indexOf(item.data('country').toString()) == -1) {
                
                is_show = false;
            } 
        }

        if(activeFilters['deliverymax']) {
            if(activeFilters['deliverymax'] < parseInt(item.data('deliverymax'))) {
                
                is_show = false;
            } 
        }


        if(is_show) {

            if(!item.is(':visible')) {
                showItem(item);
            }
        }
        else {

            hideItem(item);
        }


        // console.log('filter ' + is_show, item.data('brand'));

    });

    $('#products_wrapper').removeClass('_loading');
    

};

var showItem = function(item) {


    item
        .show()
        .removeAttr('data-hidden')
        ;


    if(item.find('input').length == 0) {

        item
            .find('.sorting_1')
            .html('<input type="checkbox" class="adminCheckboxRow" name="_id[]" value="' + item.data('id') + '" />')
            ;
    }
};

var hideItem = function(item) {

    item
        .hide()
        .attr('data-hidden', '1')
        .find('input').remove()
        ;
};


// `
// <tr role="row" class="odd">
//     <td class="sorting_1"><input type="checkbox" class="adminCheckboxRow" name="_id[]" value="26"></td>
//     <td>4361556 </td>
//     <td></td>
//     <td>Реле </td>
//     <td>RELAY </td>
//     <td>54.53 </td>
//     <td>USD </td>
//     <td>3 202.46 </td>
//     <td>6-8 нед. </td>
//     <td>0.10 </td>
//     <td>Hitachi </td>
//     <td>- </td>
//     <td>12.02.2018 17:57 </td>
//     <td></td>
//     <td>
//         <a href="http://reman.dev/products/26/edit" class="btn btn-xs btn-primary" title="Редактировать" data-toggle="tooltip">
//             <i class="fa fa-pencil"></i>
    
//             </a>
//             <form action="http://reman.dev/products/26/delete" method="POST" style="display:inline-block;">
//             <input type="hidden" name="_token" value="fu4Q8Xi0FoEXTYCOU2j7ll7QmzWUrnyB0m26ibEr">
//             <input type="hidden" name="_method" value="delete">
//             <button class="btn btn-xs btn-danger btn-delete" title="Удалить" data-toggle="tooltip">
//                             <i class="fa fa-trash"></i>
                
//                     </button>
//         </form>
//     </td>
// </tr>
// `
