$(document).ready(function(){

//    var columnTitles = ['Титул (ru)', 'Титул (en)', 'Год / годы'];
//    var columns = ['title_ru', 'title_en', 'year'];
//
//    $('#titles .row-admincolumneditabletext').each(function(i, item) {
//
//        item = $(item);
//
//        item.html(
//            $('<a />')
//            .text($.trim(item.text()))
//            .addClass('editable inline-editable')
//            .attr('href', '#')
//            .attr('data-type', i < 2 ? 'textarea' : 'text')
//            .attr('data-pk', item.closest('tr').find('._pk').data('value'))
//            .attr('data-url', '/admin/boxer_titles')
//            .attr('data-title', columnTitles[i])
//            .attr('data-name', columns[i])
//            .editable()
//        );
//    });

    var deleteBtn = $('form table td .btn-delete');

    if(deleteBtn.length) {
        deleteBtn
            .siblings('input[name="_method"]').remove().end()
            .siblings('input[name="_token"]').remove()
        ;

        deleteBtn.replaceWith(
            $('<a />')
            .addClass('btn btn-danger btn-xs btn-delete')
            .attr('href', deleteBtn.siblings('.btn-primary').attr('href').replace('edit', 'delete'))
            .html(deleteBtn.html())
            .click(function(e){

                e.preventDefault();

                if(confirm('Запись будет удалена. Выполнить удаление?')) {
                    var $el = $(this);

                    $.post($el.attr('href'), {_method: 'DELETE'}, function(data){

                        if(data) {
                            $el.closest('tr').remove();
                        }

                    });
                }

            })
        );
    }


//    $('#form_boxer_title button[type="submit"][value="save_and_continue"]').attr('value', 'save_and_close');

});