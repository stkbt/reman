$(document).ready(function(){

    Admin.Events.on('datatables::draw', function(el) {

        $(el)
            .find('tbody')
                .find('tr')
                    .find('td:eq(0)').addClass('text-muted text-small').end()
                    .find('td:eq(9)').addClass('text-hidden').end()
                    .attr('data-status', function(index, attr){

                        return $(this).find('td:eq(9)').text().trim();
                    })
                    .find('.btn-sendved')
                        .closest('form').submit(onAddRequestSubmit)
                    ;
    });

});


var onAddRequestSubmit = function(e){

    e.preventDefault();

    var url = e.target.action;

    var num = $(e.target).closest('tr').find('td:eq(1)').text().trim();

    var content = '<div class="prompt_requestadd__content"><textarea class="form-control" name="sender_message" id="sender_message" rows="3"></textarea></div>';

    swal({
        title: '<h4><b>Заявка '+num+'</b></h4><h4>Отправка запроса на добавление позиций<br/><small>Можно оставить комментарий</small></h4>',
        text: content,
        // type: 'success',
        confirmButtonText: 'Отправить запрос',
        showCancelButton: 1,
        cancelButtonText: 'Отмена',
        closeOnConfirm: 0, 
        closeOnCancel: 0,
        button: {
            text: "Search!",
            closeModal: false,
          },
    })
    .then(
        function(e) {
            
            var sender_message = $('#sender_message').val();

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    sender_message: sender_message
                },
                dataType: 'json'
            })
            .done(function (msg) {

                if(msg.success) {

                    swal({
                        title: 'Запрос успешно отправлен',
                        type: 'success',
                        showCancelButton: 1,
                        showConfirmButton: 0,
                        cancelButtonText: 'Закрыть',
                        timer: 3000
                    })
                    ;
                }
                else {

                    swal({
                        title: 'Что-то пошло не так',
                        text: 'Повторите попытку позже или обратитесь в техническую поддержку. Код ошибки [s_h.js#70]',
                        type: 'warning',
                        confirmButtonText: 'Закрыть',
                    })
                }
                
                

            });
        }, 
        function (dismiss) {
            // dismiss can be 'cancel', 'overlay','close', and 'timer'
            if (dismiss === 'cancel') {
            }
        }
    )
    ;
    
};