<?php

use Illuminate\Routing\Router;

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$router->get('/', [
    'as' => 'welcome',
    'uses' => 'WelcomeController@index'
]);

Auth::routes();

Route::get('/home', 'HomeController@index');


/**
 * Routes for images
 * 
 */ 
$router->get('attaches/{dateImg}/{filename}/{width}/{height}/{type?}/{anchor?}/{x?}/{y?}', 'ImageController@whResize');
$router->get('attaches/{dateImg}/{filename}/', 'ImageController@fullImage');
