<div class="form-buttons panel-footer text-center">
    
     <button
            type="submit"
            class="btn btn-success btn-lg">
         {{ $saveButtonText }} <i class="fa fa-paper-plane-o" style="margin-left: 12px;"></i>
    </button>

    <div style="height:36px;"></div>

    <a href="{{ $backUrl }}" class="btn btn-link">
        <i class="fa fa-ban"></i> {{ $cancelButtonText }}
    </a>

</div>