<span class="form-inline action-select">
	
	@if($pretitle)
	<span>{{ $pretitle }}</span>
	@endif

	<div class="form-group form-element-select">
	    <div>
	        {!! Form::select($select_name, $options, $select_value, ['class' => 'form-control input-select', 'data-select-type' => 'select', 'size' => 2, 'data-nullable' => true]) !!}
	    </div>
	</div>

	<button {!! $attributes !!}>
		@if ($icon)
		<i class="{{ $icon }}"></i>
		@endif

		{{ $title }}
	</button>

</span>


