@if($isEditable)
<a href="#"
   class="inline-editable"
   data-name="{{ $name }}"
   data-value="{{ $value }}"
   data-url="{{ request()->url() }}"
   data-type="text"
   data-pk="{{ $id }}"
   data-emptytext="{{ '&mdash;' }}"
   ></a>

@else
@if($value) {{ $value }} @else {{ '-' }} @endif
@endif

{!! $append !!}