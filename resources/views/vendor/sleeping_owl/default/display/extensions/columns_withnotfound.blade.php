<table {!! $attributes !!}>
    <colgroup>
        @foreach ($columns as $column)
            <col width="{!! $column->getWidth() !!}" />
        @endforeach
    </colgroup>

    <thead>
    <tr>
        @foreach ($columns as $column)
            <th {!! $column->getHeader()->htmlAttributesToString() !!}>
                {!! $column->getHeader()->render() !!}
            </th>
        @endforeach
    </tr>
    </thead>

    @yield('table.header')
    <tbody>
    @foreach ($collection as $model)
        <tr {{ $model->getKey() ? '' : 'data-nulled=1' }} data-brand="{{ $model->brand_id }}" data-country="{{ $model->country_id }}" data-deliverymax="{{ $model->delivery_time_max/86400 }}" data-id="{{ $model->id }}">
            @foreach ($columns as $column)
                <?php
                $column->setModel($model);
                if($column instanceof \SleepingOwl\Admin\Display\Column\Control) {
                    $column->initialize();
                }
                ?>

                <td {!! $column->htmlAttributesToString() !!}>
                    {!! $column->render() !!}
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>

    @yield('table.footer')
</table>

@if(!is_null($pagination))
    <div class="panel-footer">
        {!! $pagination !!}
    </div>
@endif


<div class="products_loading">
    <i class="fa fa-5x fa-notch-o fa-spin"></i>
</div>