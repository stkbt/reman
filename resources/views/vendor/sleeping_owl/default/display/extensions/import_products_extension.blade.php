<div class="import_products_extension__tabs pull-left">
	<span style="float: left;margin: 7px 12px 0 0;">Показать: </span>
	<a class="btn btn-default {{ request('is_duplicate', 'all') === 'all' ? '_current' : '' }}" href="/import_products?import_id={{ request('import_id') }}">Все</a>
	<a class="btn btn-default {{ request('is_duplicate') === '1' ? '_current' : '' }}" href="/import_products?import_id={{ request('import_id') }}&is_duplicate=1">Только совпадения</a>
	<a class="btn btn-default {{ request('is_duplicate') === '0' ? '_current' : '' }}" href="/import_products?import_id={{ request('import_id') }}&is_duplicate=0">Только новые</a>
</div> 

@if(count($actions) > 0)
<div class="pull-right">
	<form {!! $attributes !!}>
	    {{ csrf_field() }}

	    @foreach ($actions as $action)
	        {!! $action->render() !!}
	    @endforeach
	</form>
</div>
@endif
