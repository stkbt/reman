@if ( ! empty($title))
	<div class="row">
		<div class="col-lg-12">
			{!! $title !!}
		</div>
	</div>
	<br />
@endif

@yield('before.panel')

<div class="row">
	<div class="col-md-4">

		@if ($creatable)
			<a href="{{ url($createUrl) }}" class="btn btn-primary btn-lg">
				<i class="fa fa-plus"></i> {{ $newEntryButtonText }}
			</a>
		@endif
	
		
	</div>
	<div class="col-md-8">
		
		<div class="panel panel-default">
			<div class="panel-heading">

				<h4>История импорта данных</h4>

				@yield('panel.buttons')

				<div class="pull-right">
					@yield('panel.heading.actions')
				</div>
			</div>

			@yield('panel.heading')

			@foreach($extensions as $ext)
				{!! $ext->render() !!}
			@endforeach

			@yield('panel.footer')

		</div>
	</div>
</div>


@yield('after.panel')
