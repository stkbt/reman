<div id="productssearch" class="productssearch row">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="box box-info">
		    {{-- <div class="box-header with-border">
		        <h3 class="box-title">Подбор товаров</h3>

		    </div> --}}
		    <div class="box-body">
		    	
		    	@if($bill_id)
		    	<input type="hidden" name="bill_id" value="{{ $bill_id }}" />
		    	@endif

		    	<div class="row">
			    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<label for="skus" class="control-label">Добавить позицию по артикулу</label>

    			{{ Form::open(['url' => route('admin.searchAjax'), 'method' => 'POST', 'class' => 'form-inline', 'id' => 'products__form_add']) }}


					   {{--  <div class="form-group form-element-textarea">
							<label for="skus" class="control-label">Добавить по артикулу</label>
							<input class="form-control" name="skus"></input>
						</div> --}}

						<div class="form-group">
						    
							<div class="input-group">
								<input class="form-control" name="sku" placeholder="Артикул" style="width:180px;" />
							</div>
						</div>
						
						<button type="submit" class="btn btn-default" style="vertical-align: top">Найти и добавить</button>

				{{ Form::close() }}

			    	</div>
			    	
		    	</div>


		    </div>
		</div>	
	</div>
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<div class="box box-info">
		    {{-- <div class="box-header with-border">
		        <h3 class="box-title">Подбор товаров</h3>

		    </div> --}}
		    <div class="box-body">
		    	
		    	@if($bill_id)
		    	<input type="hidden" name="bill_id" value="{{ $bill_id }}" />
		    	@endif

		    	<div class="row">
			    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						{!! $brandsSelect !!}
			    	</div>
			    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						
						{!! $countriesSelect !!}

			    	</div>

			    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<label  class="control-label">Макс. срок поставки, дней</label>

						<div style="margin-top: -20px">
							<input type="text" id="delivery_time" name="delivery_time" value="" data-min="{{ $delivery_times_minmax[0] }}" data-max="{{ $delivery_times_minmax[1] }}" data-from="{{ $delivery_times_minmax[1] }}" />
						</div>
			    	</div>

			    	{{-- <div class="col-md-2">
						<div style="padding-top:23px;">
							<button type="submit" class="btn btn-info">Фильтр</button>
						</div>
			    	</div> --}}
		    	</div>


		    </div>
		</div>	
	</div>
</div>
