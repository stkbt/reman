<div class="form-inline">
	<div class="form-group form-element-radio">
		@foreach ($options as $optionValue => $optionLabel)
			<div class="radio">
				<label class="_radioformats">
					<input type="radio" name="{{ $name }}" value="{{ $optionValue }}" {!! ($value == $optionValue) ? 'checked' : '' !!}/>
					<span class="_title">{!! $optionLabel !!}</span>
				</label>
			</div>
		@endforeach
	</div>
</div>

<style>
._radioformats {
	margin-right: 24px;
	padding:8px 12px!important;
	border:1px dotted #eee;
	display: block;
	border-radius: 4px;
	font-size: 16px;
}
._radioformats:hover {
	border-style: solid;
}
._radioformats > * {
	vertical-align: bottom;
}
._radioformats input {
	margin-right: 4px;
}
._radioformats ._title {
	line-height: 1;
}
</style>