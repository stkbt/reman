<!-- Right Side Of Navbar -->
{{-- <ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Вход</a></li>
    @else
        <li class="">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Выйти
                    </a>

                    
                </li>
            </ul>
        </li>
    @endif
</ul> --}}

<div class="pull-right">
@if(!Auth::guest())
<form id="logout-form" action="{{ url('/logout') }}" method="POST">
    {{ csrf_field() }}
    <button type="submit" class="btn-logout">Выйти</button>
</form>
@endif
</div>