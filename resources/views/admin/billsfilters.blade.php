<div id="productssearch" class="productssearch row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="box box-info">
		    {{-- <div class="box-header with-border">
		        <h3 class="box-title">Подбор товаров</h3>

		    </div> --}}
		    <div class="box-body">
		    	
		    	{{-- @if($contractor_id)
		    	<input type="hidden" name="contractor_id" value="{{ $contractor_id }}" />
		    	@endif --}}

		    	{{ Form::open(['method' => 'GET']) }}

		    	<div class="row">
			    	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

						{!! $contractorSelect !!}

			    	</div>
			    	@if($userSelect)
			    	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						
						{!! $userSelect !!}

			    	</div>
			    	@endif
			    	@if($organizationSelect)
			    	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						
						{!! $organizationSelect !!}

			    	</div>
			    	@endif

			    	<div class="col-md-2">
						<div style="padding-top:21px;">
							<button type="submit" class="btn btn-info">Применить фильтр</button>
						</div>
			    	</div>
		    	</div>


		    	{{ Form::close() }}

		    	@if($description)
		    	<div class="row">
			    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			    		{!! $description !!}
			    	</div>
			    </div>
		    	@endif

		    </div>
		</div>	
	</div>
</div>
