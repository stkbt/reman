{{--  --}}

				<div class="panel panel-default">
					<div class="panel-heading">
						
						
						
						<div class="pull-right">
						</div>
					</div>
					
					<table class="table table-striped">
						<colgroup>
						<col width="">
						<col width="">
						<col width="">
						<col width="90px">
						</colgroup>
						<thead>
							<tr>
								<th class="row-header">
									Артикул
								</th>
								<th class="row-header">
									Альт. номер
								</th>
								<th class="row-header">
									Название
								</th>
								<th class="row-header">
									Название англ.
								</th>
								<th class="row-header">
									Цена
								</th>
								<th class="row-header">
									Валюта
								</th>
								<th class="row-header">
									Цена, руб.
								</th>
								<th class="row-header">
									Срок поставки
								</th>
								<th class="row-header">
									Вес
								</th>
								<th class="row-header">
									Производитель
								</th>
								<th class="row-header">
									Количество
								</th>
							</tr>
						</thead>
						<tbody>

						@if($items)
						@foreach($items as $item)
							<tr>
								<td class="row-text">{{ $item['sku'] }}</td>
								<td class="row-text">{{ $item['sku_alternate'] }}</td>
								<td class="row-text">{{ $item['title'] }}</td>
								<td class="row-text">{{ $item['title_en'] }}</td>
								<td class="row-text">{{ $item['price'] }}</td>
								<td class="row-text">{{ $item['currency_title'] }}</td>
								<td class="row-text">{{ $item['price_rub'] }}</td>
								<td class="row-text">{{ $item['delivery_time'] }}</td>
								<td class="row-text">{{ $item['weight'] }}</td>
								<td class="row-text">{{ $item['brand_title'] }}</td>
								<td class="row-text">{{ $item['count'] }}</td>
							</tr>
						@endforeach
						@endif

						@if($not_found_skus)
						@foreach($not_found_skus as $not_found_sku)
							<tr data-nulled="1">
								<td class="row-text">{{ $not_found_sku }}</td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
								<td class="row-text"></td>
							</tr>
						@endforeach
						@endif

						</tbody>
						<tfoot>
						<tr>
						</tr>
					</tfoot></table>
					<div class="panel-footer">
						
					</div>
					
				</div>

				<p style="margin-top:12px;">
					<span class="sh_desc"><strong>№ Заявки: </strong> {{ $search_history->title }} </span>
					<span class="sh_desc"><strong>Дата: </strong> {{ $search_history->created_at->format('d.m.Y H:i') }} </span>
					<span class="sh_desc"><strong>Сумма: </strong> {{ number_format($search_history->total, 2, '.', ' ') }} </span>
					<span class="sh_desc"><strong>Общий вес: </strong> {{ $search_history->weight }} </span>
					<span class="sh_desc"><strong>Комментарий: </strong> {{ $search_history->comment }} </span>
				</p>

				{{-- 
			</div>
		</div>
	</body>
</html> --}}