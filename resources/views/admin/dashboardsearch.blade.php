<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="box box-primary">
		    <div class="box-header with-border">
		        <h3 class="box-title">Поиск товаров по артикулам</h3>

		    </div>
		    <div class="box-body">
		    	{{ Form::open(['url' => route('admin.searchSubmit'), 'method' => 'POST']) }}
		    	
		    	<div class="form-group form-element-textarea">
					<label for="skus" class="control-label">Укажите артикулы товаров</label>
					<textarea class="form-control" rows="4" name="skus"></textarea>
					<p class="help-block">
					Можно указать несколько артикулов: каждый артикул с новой строки.<br/>Также доступно указание требуемого количества через пробел от артикула.
					</p>
				</div>

				<div class="form-group">
					{{-- TODO: доп фильтры --}}
				</div>

				<div class="form-group form-element-checkbox ">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="search_alternate" value="1" checked="true" />
							Искать по альтернативному номеру
						</label>
					</div>
				</div>

				{!! $brandsSelect !!}

				<hr/>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group form-element-textarea">
							<label for="history_title" class="control-label">№ Заявки</label>
							<input type="text" name="history_title" class="form-control" placeholder="{{ \App\SearchHistory::generateTitle() }}" />
						</div>
					</div>
					<div class="col-md-6">
						{!! $contractorSelect !!}
					</div>
				</div>

				<hr/>

				<div class="form-group text-center">
					<button type="submit" class="btn btn-primary btn-block">Искать</button>
				</div>

				{{ Form::close() }}

		    </div>
		</div>	
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="box box-success">
		    <div class="box-header with-border">
		        <h3 class="box-title">Поиск товаров по наименованию</h3>

		    </div>
		    <div class="box-body">
		    	{{ Form::open(['url' => route('admin.searchSubmitByTitle'), 'method' => 'POST']) }}
		    	
		    	<div class="form-group form-element-textarea">
					<label for="title" class="control-label">Укажите искомое наименование товаров</label>
					<input type="text" name="title" class="form-control" />
				</div>

				<div class="form-group">
					{{-- TODO: доп фильтры --}}
				</div>

				{!! $brandsSelect !!}

				<hr/>

				<div class="form-group text-center">
					<button type="submit" class="btn btn-success btn-block">Искать</button>
				</div>

				{{ Form::close() }}

		    </div>
		</div>	
	</div>

</div>

<hr style="border-top-color:#ddd;"/>

<div class="row">
	<div class="col-md-6 col-xs-12">
		<p class="text-muted">Курсы валют <small>/ обновлены {{ $currencies->first()->updated_at->format('d.m.Y H:i') }} / <a href="{{ route('admin.updatecurrencies') }}">обновить сейчас</a></small></p>
		<p class="text-muted">
			@foreach($currencies as $currency)
			<span style="margin-right: 24px;display: inline-block;">{{ $currency->title }}: {{ $currency->rate }}</span>
			@endforeach
		</p>
	</div>
</div>
