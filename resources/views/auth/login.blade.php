@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="auth_panel panel panel-default">
                <div class="panel-heading text-center">
                    <img src="/assets/logo.png" height="48" />
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Запомнить меня
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-warning">
                                    Войти
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Забыли пароль?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <p>Для доступа к бета-версии исользуйте следующие реквизиты:</p>
            <ul>
                <li>
                    Администратор
                    <ul>
                        <li>логин: admin@reman-service.ru</li>
                        <li>пароль: admin123</li>
                    </ul>
                </li>
                <li>
                    Менеджер 1
                    <ul>
                        <li>логин: manager1@reman-service.ru</li>
                        <li>пароль: manager123</li>
                    </ul>
                </li>
                <li>
                    Менеджер 2
                    <ul>
                        <li>логин: manager2@reman-service.ru</li>
                        <li>пароль: manager123</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div> --}}

@endsection
