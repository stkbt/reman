<h3>Новый запрос на добавление позиций</h3>
<a href="{{ url('add_requests/' . $addrequest->id . '/edit') }}">Просмотр запроса</a>
<p>&nbsp;</p>
<h4>Заявка: <a href="{{ url('search_histories?id=' . $addrequest->search_history->id) }}">{{ $addrequest->search_history->title }}</a></h4>
<h4>Менеджер: {{ $addrequest->sender->name }}, {{ $addrequest->sender->email }}, {{ $addrequest->sender->phone }}</h4>
<h4>Комментарий менеджера:</h4>
<p>{!! $addrequest->sender_message ? nl2br($addrequest->sender_message) : '–' !!}</p>
<p>&nbsp;</p>
<p>Полная заявка во вложении к данному письму</p>