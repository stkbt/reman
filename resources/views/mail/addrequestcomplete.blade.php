<h3>Запрос на добавление позиций обработан</h3>
<h4>Заявка: <a href="{{ url('search_histories?id=' . $addrequest->search_history->id) }}">{{ $addrequest->search_history->title }}</a></h4>
<h4>Комментарий:</h4>
<p>{!! $addrequest->handler_message ? nl2br($addrequest->handler_message) : '–' !!}</p>
<p>&nbsp;</p>
<p>Исходная заявка во вложении к данному письму</p>