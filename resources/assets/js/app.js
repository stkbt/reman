
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

//Vue.component('example', require('./components/Example.vue'));
//
//const app = new Vue({
//    el: '#app'
//});


window.$ = window.jQuery = require('jquery');



// $('#products').find('td').on('click', function(e){


// 	$(this).closest('tr').find('input[type="checkbox"]').attr('checked', true).trigger('change');
// });



// $('#createOrder').on('click', function(e){

// 	e.preventDefault();

// 	alert(1);

// });


$(() => {
    
    window.csrfToken = $('meta[name=csrf-token]').attr('content');

	// if(location.search.indexOf('redirect=back') != -1) {

	// 	$('button[type="submit"][value="save_and_close"]')
	// }


		$('#btnAddBillProducts').on('click', function(e){

			e.preventDefault();

			if(!$('#modalAddBillProducts').length) {

				$('<div />')
					.attr('id', 'modalAddBillProducts')
					.addClass('modal fade bs-example-modal-sm')
					.attr('tabindex', '-1')
					.attr('role', 'dialog')
					.append(
						$('<div />')
							.addClass('modal-dialog modal-md')
							.attr('role', 'document')
							.append(
								$('<div />')
									.addClass('modal-content')
									.append(
										$('<div />')
											.addClass('modal-header')
											.append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>')
											.append('<h4 class="modal-title" id="">Добавление позиций в счёт</h4>')
									)
									.append(
										$('<div />')
											.addClass('modal-body')
											.append('' +
												'<form method="POST" action="/searchSubmit?bill_id='+$('input[name="id"]').val()+'" accept-charset="UTF-8">' + 
													'<input name="_token" type="hidden" value="' + window.csrfToken + '">' +
				    								'<div class="form-group form-element-textarea">' +
														'<label for="skus" class="control-label">Укажите артикулы товаров</label>' +
														'<textarea class="form-control" rows="5" name="skus"></textarea>' +
														'<p class="help-block">Можно указать несколько артикулов: каждый артикул с новой строки. Также для разделения артикулов можно использовать запятую или пробел.</p>' +
													'</div>' +
													'<div class="form-group"><button type="submit" class="btn btn-success btn-lg">Искать</button></div>' +
												'</form>')
									)
							)
					)
					.appendTo($('body'))

			}

			$('#modalAddBillProducts').modal();
		});


		var updateBillData = function(fields, complete) {

			if(typeof(fields) == 'undefined') fields = {};

			$.get('/billData', {id: $('input[name="id"]').val(), fields: fields}, function(data){

		    	for(var it in data) {
		    		if(it != 'fields')
		    			$('#' + it).text(data[it]);
		    	}

		    	if(data['fields']) {
			    	for(var it in data['fields']) {
			    		$('[name="' + it + '"]').val(data['fields'][it]);
			    	}
			    }


		    }, 'json');
		};


		if($('.billitems_in_bill').length) {

			$('.billitems_in_bill').find('.editable').each(function(i, item){

				item = $(item);

				var _id = $(item).attr('data-pk');

				$(this)
					.attr('data-url', '/billitems')
					.editable('destroy')
					.editable({
	                    ajaxOptions: {
	                        url: '/billitems',
	                        success: function(response, newValue) {
							    
							    $.get('/billitemData', {id: _id}, function(data){

							    	for(var it in data) {
							    		item.closest('tr').find('.__' + it).text(data[it]);
							    	}

							    	updateBillData();

							    }, 'json');
							}
	                    }
	                })
				;

			});

		}


		if($('#billcurrency').length) {

			$('#billcurrency').find('input[type="radio"]').on('change', function(e){

				updateBillData({currency_id: $(this).val()});
			});	
		}


		if($('#bill_with_nds').length) {

			$('#bill_with_nds').find('input[type="radio"]').on('change', function(e){

				if($(this).is(':checked')) {

					updateBillData({with_nds: $(this).val()});
				}
			});	
		}



		$('#contractor_id').on('change', function(e){

			updateBillData({contractor_id: $(this).val()});
		});


		$('#organization_id').on('change', function(e){

			updateBillData({organization_id: $(this).val()});
		});


		$('.form-element-helptext .checkbox input[type="checkbox"]').on('change', function(e){

			var input = $(this).closest('.form-element-helptext').prev('input');

			if($(this).is(':checked')) {
				input.removeAttr('readonly');
			}
			else {
				input.attr('readonly', true);
			}

			if($(this).attr('name') == 'custom_delivery_time') {

				updateBillData({custom_delivery_time: $(this).is(':checked') ? 1 : 0});
			}

		});

		$('#billform').find('input[type="text"], textarea').on('blur', function(e){

			var data = {};

			data[$(this).attr('name')] = $(this).val();

			updateBillData(data);
		});




    // Admin.Modules.add('display.myactions', () => {

    	var resetSelectedProducts = function() {

    		$('.adminCheckboxRow').filter(':checked').removeAttr('checked').trigger('change');
    	};

		$('form.products_actions[data-type="display-actions"]').off('submit');  	


	    $(document).on('click', 'form.products_actions .btn, .add_to_bill', function (e) {

	    	e.preventDefault();


	        var $btn = $(this);

	    	if($btn.hasClass('add_to_bill')) {

	    		$btn.closest('tr').find('input[type="checkbox"]').attr('checked', true).trigger('change');
	    	}

	        var $checkboxes = $('.adminCheckboxRow').filter(':checked'),
	            data = Array(),
	            resultText = 'Перейдите к счёту для выбора контрагента и совершения дополнительных действий'
	            ;

	        
	        if (!$checkboxes.length) {
	            swal(
	                '',
	                'Выберите один или несколько товаров для добавления их к счёту',
	                'error'
	            )
	            e.preventDefault();
	            return;
	        }


	        $checkboxes.each(function(){

	        	data.push({id: $(this).val(), count: parseInt($.trim($(this).closest('tr').find('._count').text()))});
	        });

			var bill_id = null;      

	        switch($btn.attr('id')) {
	        	case 'createOrder':

	        	break;
	        	case 'addToBill':

	        	bill_id = $('form[data-type="display-actions"]').find('select[name="bill_id"]').val();

	        	resultText = 'Вы можете перейти к счёту или продолжить работу с товарами';
	        	
	        	break;
	        }

	        url = $btn.data('action');


	        

	        if($btn.hasClass('add_to_bill')) {
	        	bill_id = $btn.attr('data-bill_id');
	        	// delete data['_id[]'];
	        	resultText = 'Вы можете перейти к счёту или продолжить работу с товарами';
	        	url = $btn.attr('href');
	        }

	        $.ajax({
	            type: $btn.data('method'),
	            url: url,
	            data: {data: data, bill_id: bill_id},
	            dataType: 'json'
	        })
            .done(function (msg) {

            	if(msg.success) {

            		swal({
	                    title: 'Товары добавлены к счёту',
	                    text: resultText,
	                   	type: 'success',
	                   	confirmButtonText: 'Перейти к счёту',
	                   	showCancelButton: 1,
	                   	cancelButtonText: 'Назад к поиску',
	                   	closeOnConfirm: 0, 
	                   	closeOnCancel: 0
	                })
	                .then(
	                	function() {
	                	 
	                	 	location.href = '/bills/' + msg.bill_id + '/edit';
	                	}, 
	                	function (dismiss) {
						 	// dismiss can be 'cancel', 'overlay','close', and 'timer'
						 	if (dismiss === 'cancel') {
						 	}

						 	resetSelectedProducts();
						 	$('input[name="skus"]').focus();
						}
	                )
	                ;
            	}
            	else {

            		swal({
	                    title: 'Что-то пошло не так',
	                    text: 'Повторите попытку позже или обратитесь в техническую поддержку. Код ошибки #315',
	                   	type: 'warning',
	                   	confirmButtonText: 'Закрыть',
	                })
            	}
                
                

            });

	        return false;
	    });
	// });




	$('form.import_products_actions[data-type="display-actions"]').off('submit');  	


	var $icon_loading = $('<i />').addClass('fa fa-circle-o-notch fa-spin');

    $(document).on('click', 'form.import_products_actions .btn', function (e) {

    	e.preventDefault();


    	var $btn = $(this);

    	var $icon = $btn.find('i');

        var $checkboxes = $('.adminCheckboxRow').filter(':checked'),
            data = Array()
            ;

        
        if (!$checkboxes.length) {
            swal(
                '',
                'Выберите один или несколько товаров для совершения действия',
                'error'
            )
            e.preventDefault();
            return;
        }


        $checkboxes.each(function(){

        	data.push({id: $(this).val(), count: parseInt($.trim($(this).closest('tr').find('._count').text()))});
        });


        $icon.hide();
        $btn.prepend($icon_loading);

        $.ajax({
	            type: $btn.data('method'),
	            url: $btn.data('action'),
	            data: {data: data},
	            dataType: 'json'
	        })
            .done(function (msg) {

            	$icon.show();
            	$icon_loading.remove();

            	if(msg.success) {

            		$checkboxes.each(function(){

            			$(this).closest('tr').remove();
            		});

            	}
            	else {

            		swal({
	                    title: 'Что-то пошло не так',
	                    text: 'Повторите попытку позже или обратитесь в техническую поддержку. Код ошибки #394',
	                   	type: 'warning',
	                   	confirmButtonText: 'Закрыть',
	                });
            	}
                
                

            });


    });


    $('form.import_products_extension[data-type="display-actions"]').off('submit'); 


    $(document).on('click', 'form.import_products_extension .btn', function (e) {

    	e.preventDefault();

    	var $btn = $(this);

    	swal.queue([{
			title: 'Импорт позиций',
			confirmButtonText: 'Продолжить',
			text:'Все позиции будут импортированы',
			showCancelButton: true,
			cancelButtonText: 'Отмена',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve) {
				  $.post($btn.data('action'))
				    .done(function (data) {
				      	
				      	swal({
		                    title: 'Успех',
		                    text: 'Все позиции были импортированы',
		                   	type: 'success',
		                   	confirmButtonText: 'Закрыть',
		                })
		                .then(
		                	function() {
		                	 
		                	 	location.href = '/products';
		                	}
		                )
				      
				    });
				});
			}
		}]);	

    });




	var deleteBtn = $('#table_in_form td .btn-delete');
    
    if(deleteBtn.length) {
        
        deleteBtn
            .siblings('input[name="_method"]').remove().end()
            .siblings('input[name="_token"]').remove()
        ;


        deleteBtn.each(function(i, item){

        	$(this).replaceWith(
	            $('<a />')
	            .addClass('btn btn-danger btn-xs btn-delete')
	            .attr('href', '/billitems/' + $(this).closest('tr').find('._id').text().trim() + '/delete')
	            .html(deleteBtn.html())
	            .click(function(e){

	                e.preventDefault();
	                
	                if(confirm('Позиция будет удалена. Продолжить?')) {
	                    
	                    var $el = $(this);

	                    $.post($el.attr('href'), {_method: 'DELETE'}, function(data){

	                        if(data) {
	                            $el.closest('tr').remove();
	                        }

	                        if($('#billcurrency').length) {		//check if we in bill

	                        	updateBillData();
	                        }

	                    });
	                }
	                
	            })
	        );
        });
    }


    var sh_modal;

    $(document).on('click', '#search_histories .btn-view', function (e) {

    	e.preventDefault();

    	if(!sh_modal) {

    		var modal_str = '<div class="modal fade search_histories_modal" tabindex="-1" role="dialog" aria-labelledby="search_histories">' +
					  			'<div class="modal-dialog modal-lg" role="document">' +
								    '<div class="modal-content">' +
								    	'<div class="modal-header">' +
									        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
									        '<h4 class="modal-title" id="myModalLabel">Результаты истории поиска</h4>' +
									      '</div>' +
									      '<div class="modal-body" id="search_histories_content"></div>' +
								    '</div>' +
								'</div>' +
							'</div>';

    		sh_modal = $(modal_str).appendTo('body');
    	}	

    	$.get($(this).closest('form').attr('action'), {}, function(data){

    		$('#search_histories_content').html(data);
    	});

    	sh_modal.modal();
    	

    });

});

