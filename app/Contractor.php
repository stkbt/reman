<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Scopes\UserScope;

use Auth;

class Contractor extends Model
{

	protected static function boot() {

		self::creating(function($item){

			$item->user_id = Auth::id();
		});

        parent::boot();

        static::addGlobalScope(new UserScope);

    }
    
    public function user() {

    	return $this->belongsTo('App\User');
    }

    public function bills() {

    	return $this->hasMany('App\Bill');
    }


    public function getTitleInnAttribute() {

        return $this->title . ($this->inn ? ' | ' . $this->inn : '');
    }
}
