<?php

namespace App\ServiceModels;

use Image;
use Storage;

trait ImagableTrait {
    
    
    public function setImagable($field, $filename, $parameters = ['width' => 2000, 'height' => null]) {

        if(!$filename) {
            $this->attributes[$field] = null;
            return;
        }
        
        if(!isset($parameters['width'])) $parameters['width'] = 2000;
        if(!isset($parameters['height'])) $parameters['height'] = null;
        
        
        $path = 'attaches/'.date ('Y-m-d').'/';

        if(isset($this->attributes[$field]) && $filename == $this->attributes[$field]) return;

        if(is_file(public_path($filename))) {

            $info = pathinfo($filename);

            $file = Image::make(public_path($filename))
                ->resize(2000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save()
                ;

            $newFileName = md5(date('Y-m-d-s').$filename).'.'.$info['extension'];

            Storage::put(
                $path.$newFileName,
                file_get_contents(public_path($filename))
            );

            @unlink(public_path($filename));

            $this->attributes[$field] = $path.$newFileName;
        }
        else {
            $this->attributes[$field] = null;
        }

    }
}