<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Excel;

use App\Scopes\BillScope;
use App\Billitem;


class Bill extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected static function boot() {

		self::creating(function($item){

			$item->user_id = Auth::id();
		});

        parent::boot();

        static::addGlobalScope(new BillScope);

    }


	protected $fillable = [
        'user_id', 'contractor_id', 'is_draft', 'items', 'search_history_id'
    ];
    
    public function user() {

    	return $this->belongsTo('App\User');
    }
    public function contractor() {

        return $this->belongsTo('App\Contractor');
    }
    public function currency() {

    	return $this->belongsTo('App\Currency');
    }
    public function items() {

    	return $this->hasMany('App\Billitem');
    }

    public function organization() {

        return $this->belongsTo('App\Organization');
    }

    public function search_history() {

        return $this->belongsTo('App\SearchHistory');
    }


    public function getTitleAttribute() {

        $title = '№' . $this->id;

        if($this->contractor) {

            $title .= ' / ' . $this->contractor->title;
        }
        else {

            $title .= ' / -';
        }

        return $title . ' / ' . date('d.m.Y H:i', strtotime($this->created_at));
    }

    public function getTitleShortAttribute() {

        $title = '№' . $this->id;

        return $title . ' / ' . date('d.m.Y', strtotime($this->created_at));
    }


    public function getItemsArrayAttribute() {

        $items = [];

        foreach($this->items as $item) {

            $items[] = [
                'Наименование' => $item->title,
                'Артикул'   => $item->product->sku,
                ('Цена, ' . $this->currency->title) => $item->priceCurFormatted
            ];
        }

        return $items;
    }

    public function addItems($items) {

        if (!$items) return;
        if (!$this->exists) $this->save();

        $now = date('Y-m-d H:i:s');
        
        $items = $items->map(function($item) use ($now){
            
            return [
                'product_id' => $item->id,
                'title' => $item->title,
                'bill_id'   => $this->id,
                'price'   => $item->price,
                'currency_id'   => $item->currency_id,
                'count' => $item->count,
                'created_at' => $now,
                'updated_at' => $now,
                'delivery_time_min' => $item->delivery_time_min,
                'delivery_time_max' => $item->delivery_time_max,
            ];
            
        })->all();
        
        Billitem::insert($items);
    }

    public function setItemsAttribute($items) {
        
        if (!$items) return;
        if (!$this->exists) $this->save();

        //TODO: clear existings ??
        
        $now = date('Y-m-d H:i:s');
        
        $items = $items->map(function($item) use ($now){
            
            return [
                'product_id' => $item->id,
                'title' => $item->title,
                'bill_id'   => $this->id,
                'price'   => $item->price,
                'currency_id'   => $item->currency_id,
                'count' => $item->count,
                'created_at' => $now,
                'updated_at' => $now,
                'delivery_time_min' => $item->delivery_time_min,
                'delivery_time_max' => $item->delivery_time_max,
            ];
            
        })->all();
        
        Billitem::insert($items);
    }


    public function getItemsCountAttribute() {

        return $this->items->count();
    }

    public function getTotalWeightAttribute() {

        if(!$this->items || $this->items->count() == 0) {
            return 0;
        }

        return $this->items->sum(function($item){

            return $item->weight*$item->count;
        });
    }
    public function getTotalRubAttribute() {

        if(!$this->items || $this->items->count() == 0) {
            return 0;
        }

        $total = $this->items->sum('totalRub');

        if($this->with_nds == 0) {

            $total = $total/1.18;
        }

    	return $total;
    }
    public function getTotalRubFormattedAttribute() {

    	return number_format($this->totalRub, 2, ',', ' ');
    }

    public function getTotalCurAttribute() {

        return $this->totalRub / $this->currency->rate;
    }
    public function getTotalCurFormattedAttribute() {

        return number_format($this->totalCur, 2, ',', ' ');
    }

    public function getDeliveryTimeAttribute() {

        if(isset($this->attributes['delivery_time'])) {
            $dt = $this->attributes['delivery_time'];
        }
        else {
            $dt = '–';
        }

        if(!$this->custom_delivery_time) {

            $delivery_time_max = 0;
            $item_max = null;

            foreach ($this->items as $item) {
                if(isset($item->delivery_time_max) && $item->delivery_time_max > $delivery_time_max) $item_max = $item;

                if(is_null($item->delivery_time_max) && is_null($item->delivery_time_max)) return '–';
            }

            if($item_max) return $item->delivery_time;
            
            if($delivery_time_max == 0) return 'в наличии';
        }

        return $dt;
    }

    public function getNumberFormattedAttribute() {

        if($this->number) return str_slug($this->number);

        return str_slug($this->id);
    }

    public function getFilenameAttribute() {

        $filename = 'KP_' . $this->numberFormatted . '_' . date('d-m-Y', strtotime($this->created_at));

        return $filename;
    }


    public function getPaymentmethodAttribute() {

        if(isset($this->attributes['paymentmethod'])) return $this->attributes['paymentmethod'];

        if($this->contractor)
            return $this->contractor->default_paymentmethod;
    }
    public function getDeliveryConditionsAttribute() {

        if(isset($this->attributes['delivery_conditions'])) return $this->attributes['delivery_conditions'];

        if($this->contractor)
            return $this->contractor->default_delivery_conditions;
    }
    public function getNotesAttribute() {

        if(isset($this->attributes['notes'])) return $this->attributes['notes'];

        if($this->contractor)
            return $this->contractor->default_notes;
    }


    public function getWithNdsTitleAttribute() {

        return $this->with_nds ? 'вкл. НДС 18%' : 'без НДС';
    }



    public function generateFileExport($format = 'xls', $action = 'download') {

        $bill = $this;

        $template = storage_path('templates/kp.xls');
        $signtext = 'Коммерческий директор ООО "Реман-Сервис" ' . "\n" . 'Нагорный Владимир Сергеевич';


        if($this->organization) {

            if($this->organization->form_file) {

                $form_file = public_path($this->organization->form_file);

                if(is_file($form_file)) {

                    $template = $form_file;
                }
            }

            $signtext = $this->organization->director;
        }


        $export = Excel::load($template, function($reader) {

            $reader->noHeading();

        });

        $sheet = $export->getSheet();

        $sheet->setCellValue('C2', $bill->number);
        $sheet->setCellValue('C3', $bill->contractor ? $bill->contractor->title : '');
        $sheet->setCellValue('C4', $bill->technics);
        $sheet->setCellValue('C6', $bill->user ? $bill->user->name : '');
        $sheet->setCellValue('C7', $bill->user ? $bill->user->phone : '');
        
        $sheet->setCellValue('C8', $bill->user ? $bill->user->email : '');

        if($bill->user) {
            $sheet->getCell('C8')->getHyperlink()->setUrl('<a href="mailto:'.$bill->user->email.'">' . $bill->user->email . '</a>');
        }
        
        
        $sheet->setCellValue('C10', $bill->delivery_time);
        $sheet->setCellValue('C11', $bill->paymentmethod);
        $sheet->setCellValue('C12', $bill->notes);
        $sheet->setCellValue('C13', $bill->delivery_conditions);


        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
            'font' => [
                'size' => 10
            ]
        ];


        $data = [];

        $num = 0;
        $row_start = 16;

        //$sheet->fromArray($data, null, 'A1', true);
        //->applyFromArray($style)

        foreach ($bill->items as $item) {
            
            

            $num++;

            $data[] = [
                $num,
                $item->title,
                $item->product->sku,
                $item->product->sku_alternate,
                $item->count,
                $item->price_cur_formatted,
                $item->total_cur_formatted,
                ($item->product->brand ? $item->product->brand->title : ''),
                $item->delivery_time
            ];
        }

        $sheet->fromArray($data, null, 'A' . $row_start, true);

        $sheet
            ->getStyle('A16:I' . ($row_start + $num - 1))
            ->applyFromArray($style)
            ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
            ;


        $sheet
            ->setCellValue('G' . ($row_start + $num), $bill->total_cur_formatted)
            ->getStyle('G' . ($row_start + $num))
            ->applyFromArray($style)
            ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
            ;

        $sheet
            ->setCellValue('F14', 'Цена ' . $bill->currency->title . ($bill->with_nds ? ' с НДС' : ' без НДС'))
            ->setCellValue('G14', 'Сумма ' . $bill->currency->title . ($bill->with_nds ? ' с НДС' : ' без НДС'));


        $signcoord = [$row_start + $num + 6, $row_start + $num + 7];




        $sheet
            ->mergeCells('H' . $signcoord[0] . ':I' . $signcoord[1])
            ->setCellValue('H' . $signcoord[0], $signtext . "\n")
            ->getStyle('H' . $signcoord[0])
            ->applyFromArray([
                'alignment' => [
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ],
                'font' => [
                    'size' => 10,
                    'bold' => true
                ]
            ])
            ->getAlignment()->setWrapText(true)
            ;

        // $sheet->getRowDimension($signcoord[0] . '')->setRowHeight(30);

        // $highestColumn = \PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

        // while ($highestColumn > 12) {
            
        //     $sheet->removeColumnByIndex($highestColumn);

        //     $highestColumn--;
        // }


        // dump($sheet->getHighestColumn());

        // for($i=11; $i < 256; $i++) {

        //     $sheet->removeColumn('J');
        // }
        

        // dump($sheet->getHighestColumn());
        
        // dd(\PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn()));


        /* fix wtf red border */
        $k = 1;
        while($k < 14) {
            $sheet
                ->getStyle('A' . $k)
                ->getBorders()
                ->getLeft()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }
        $k = 1;
        while($k < 14) {
            $sheet
                ->getStyle('I' . $k)
                ->getBorders()
                ->getRight()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }
        $k = 0;
        while($k < 9) {
            $sheet
                ->getStyleByColumnAndRow($k, 1)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }





        if($format == 'pdf') {

            // $sheet->setShowGridLines(true);

            // $sheet->getPageSetup()
            //     ->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_SMALL);

            $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

            $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            // $sheet->getPageSetup()->setFitToPage(true);
            // $sheet->getPageSetup()->setScale(300);


            // $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'PDF');

            // $sheet->getPageSetup()->setPrintArea('A1:D4');


            // $sheet->mergeCells('J1:IV' . ($row_start + $num + 7));
            
            // $sheet->setCellValue('J1', 'x');
            // $sheet->getColumnDimension('J')->setAutoSize(true);


            // $images = $sheet->getDrawingCollection();

            // foreach ($images as &$dr_image) {
                
            //     $image = $dr_image->getImageResource();
            //     // imagejpeg($image, public_path('uploads/' . $dr_image->getIndexedFilename()));

            //     $dr_image->setPath('/');
            // }

            // dd($images);

            //test from html

            $path = 'app/exports/'.date ('Y-m-d');
            $filename = md5(date('Y-m-d-s') . $bill->filename);

            $export->setFileName($filename)->store('html', storage_path($path));

            $stylesheet = "<style>td { padding-top:2px; padding-bottom:2px; padding-left:4px; } img { position:fixed;top:0;left:0;width:100px; }</style>";

            $mpdf = new \mPDF('utf-8', 'A4-L');
            $html = file_get_contents(storage_path($path . '/' . $filename . '.html'));
            
            $mpdf->WriteHTML($stylesheet);


            // $mpdf->Image(public_path('assets/logo_kp.jpg'),165,0,106,35,'jpg','',true, true, false);
            
            $mpdf->WriteHTML($html);
            
            // $mpdf->WriteHtml('<img src="'.public_path('assets/logo_kp.jpg').'" style="" />', 2);
            

            switch ($action) {
                case 'download':
                    
                    return $mpdf->Output($bill->filename, 'D');

                    return true;
                case 'store':

                    $path = 'app/exports/'.date ('Y-m-d');
                    $filename = md5(date('Y-m-d-s') . $bill->filename);

                    $mpdf->Output(storage_path($path . '/' . $filename . '.' . $format), 'F');

                    return $path . '/' . $filename . '.' . $format;
            }
        }
        
        switch ($action) {
            case 'download':
                
                $export->setFileName($bill->filename)->export($format);

                return true;
            case 'store':

                $path = 'app/exports/'.date ('Y-m-d');
                $filename = md5(date('Y-m-d-s') . $bill->filename);

                $export->setFileName($filename)->store($format, storage_path($path));

                return $path . '/' . $filename . '.' . $format;
        }
        
    }
}
