<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Utils;

class ImportProduct extends Product
{
    protected $guarded = [];


    protected static function boot() {


        parent::boot();
       
        self::creating(function($item){

       		$item->is_duplicate = isset($item->duplicate); 

            if($item->sku)
                $item->sku = Utils::cleanSku($item->sku);
            
            if($item->sku_alternate)
                $item->sku_alternate = Utils::cleanSku($item->sku_alternate);
        });

        static::addGlobalScope('duplicate', function($query){

        	$query->with('duplicate');
        });

    }

    // public function getDuplicateHashAttribute() {

    // 	return $this->sku . $this->brand_id;
    // }

    public function duplicate() {

    	return $this->belongsTo('App\Product', 'hash', 'hash');
    }

    public function getHasDuplicateAttribute() {

    	return isset($this->duplicate);
    }
}
