<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Currency;
use Request;
use Response;

use DOMDocument;

class UpdateCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatecurrencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command update currency from cbr.ru';

    
    private $cbrlist = [];
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->getcurrencies()) {
            
            $currencies = Currency::all();

            foreach($currencies as $currency) {

                if($currency->slug == 'rur') continue;

                if($rate = $this->getfromlist($currency->slug)) {
                    
                    $currency->rate = $rate;
                    $currency->touch();
                    $currency->save();
                }
            }
               
        }
        else {
            
            usleep(5000);
            
            exec('php artisan updatecurrencies');
        }
        
    }
    
    
    private function getcurrencies() {
        
        $xml = new DOMDocument();

        $date = date('d.m.Y', strtotime('+1 day', time()));

        $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $date;


        if (@$xml->load($url))
        {
            $this->cbrlist = array(); 

            $root = $xml->documentElement;
            $items = $root->getElementsByTagName('Valute');

            foreach ($items as $item)
            {
                $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
                $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
                $this->cbrlist[$code] = floatval(str_replace(',', '.', $curs));
            }

            return true;
        } 
        else
            return false;
    }
    
    public function getfromlist($cur)
    {
        $cur = strtoupper($cur);
        
        return isset($this->cbrlist[$cur]) ? $this->cbrlist[$cur] : 0;
    }
}
