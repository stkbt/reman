<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Product;

class ProductsRehash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:rehash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command create hash for products';

    
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('== Start rehash');
        
        $products = Product::all();
        foreach ($products as $product) {
            $product->hash = $product->hashGen;
            $product->save();
        }
        
        $this->info('== Complete. Have a nice day :)');
    }
}
