<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use App\Product;

class ProductsResku extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:resku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command re-create sku for products';

    
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('== Start rehash');
        
        $products = Product::all();
        foreach ($products as $product) {

            if($product->sku)
                $product->sku = \Utils::cleanSku($product->sku);

            if($product->sku_alternate)
                $product->sku_alternate = \Utils::cleanSku($product->sku_alternate);
            
            $product->save();
        }
        
        $this->info('== Complete. Have a nice day :)');
    }
}
