<?php

namespace SleepingOwl\Admin\Display;

use SleepingOwl\Admin\Display\DisplayDatatablesAsync;
use SleepingOwl\Admin\Display\DisplayDatatables;
use SleepingOwl\Admin\Display\Column\Link;
use SleepingOwl\Admin\Display\Column\Text;
use SleepingOwl\Admin\Display\Column\Email;
use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use SleepingOwl\Admin\Display\Extension\Columns;
use SleepingOwl\Admin\Display\Extension\ColumnFilters;

class DatatablesAsyncExtended extends DisplayDatatablesAsync
{

	protected $searchableColumns = [
        // Text::class,
        // Link::class,
        // Email::class,
        AdminColumnEditableText::class   /// => search by number only
    ];


    public function __construct($column_view = null) {

        parent::__construct();

        if($column_view) {

        	$this->getColumns()->setView($column_view);


            $this->column_view = $column_view;

            $this->extend('columns', new Columns());
        	$this->extend('column_filters', new ColumnFilters());
        }


        //TODO: make with status from column view (for async)
    }


    /**
     * @return Collection
     * @throws \Exception
     */
    // public function getCollection()
    // {
    //     if (! $this->isInitialized()) {
    //         throw new \Exception('Display is not initialized');
    //     }

    //     if (! is_null($this->collection)) {
    //         return $this->collection;
    //     }

    //     $query = $this->getRepository()->getQuery();

    //     $this->modifyQuery($query);


    //     $this->collection = $this->usePagination()
    //         ? $query->paginate($this->paginate, ['*'], $this->pageName)->appends(request()->except($this->pageName))
    //         : $query->get();




    //     // if(count($this->searchers)) {
    //     //     foreach ($this->searchers as $field => $values) {
    //     //         if(!is_array($values)) $values = [$values];

    //     //         foreach ($values as $value) {
                    
    //     //             if($this->isItemFoundInCollection($this->collection, $field, $value)) {

    //     //                 continue;
    //     //             }

    //     //             $item = new $this->modelClass([$field => $value]);

    //     //             $this->collection->push($item);
    //     //         }


    //     //         $this->collection = $this->collection->sortBy(function($model) use ($field, $values){
    //     //             // dump(array_search($model->$field, $values));
    //     //             return array_search($model->$field, $values);
    //     //         });
    //     //     }
    //     // }

    //     $this->getColumns()->setView($this->column_view);

    //     return $this->collection;
    // }


    // public function usePagination()
    // {
    //     return true;
    // }

}