<?php

namespace SleepingOwl\Admin\Display;

use SleepingOwl\Admin\Display\DisplayDatatables;
use SleepingOwl\Admin\Display\Extension\Columns;
use SleepingOwl\Admin\Display\Extension\ColumnFilters;

class DatatablesWithNotFound extends DisplayDatatables
{

	protected $searchers = [];
	protected $fieldsAdditional = [];
    
    protected $column_view = 'display.extensions.columns_withnotfound';


    public function __construct($column_view = null) {

        parent::__construct();

        if($column_view) {
            $this->column_view = $column_view;
        }

        $this->extend('columns', new Columns());
        $this->extend('column_filters', new ColumnFilters());
    }


    /**
     * @return Collection
     * @throws \Exception
     */
    public function getCollection()
    {
        if (! $this->isInitialized()) {
            throw new \Exception('Display is not initialized');
        }

        if (! is_null($this->collection)) {
            return $this->collection;
        }

        $query = $this->getRepository()->getQuery();

        $this->modifyQuery($query);

        $this->collection = $this->usePagination()
            ? $query->paginate($this->paginate, ['*'], $this->pageName)->appends(request()->except($this->pageName))
            : $query->get();




        if(count($this->searchers)) {
        	foreach ($this->searchers as $field => $values) {
        		if(!is_array($values)) $values = [$values];

        		foreach ($values as $value) {
        			
        			if($this->isItemFoundInCollection($this->collection, $field, $value)) {

        				continue;
        			}

        			$item = new $this->modelClass([$field => $value]);

        			$this->collection->push($item);
        		}


        		$this->collection = $this->collection->sortBy(function($model) use ($field, $values){
        			// dump(array_search($model->$field, $values));
		            return array_search($model->$field, $values);
		        });
        	}
        }

        $this->getColumns()->setView($this->column_view);

        return $this->collection;
    }


    public function setSeachers($field, $values) {

    	$this->searchers[$field] = $values;
    }
    
    public function setSeachersFieldAdditional($field, $field_additionaly) {

    	$this->fieldsAdditional[$field] = $field_additionaly;
    }

    protected function isItemFoundInCollection($collection, $field, $value) {

    	return $collection->search(function ($item, $key) use ($field, $value) {
		    
		    if(isset($this->fieldsAdditional[$field])) {

		    	$afield = $this->fieldsAdditional[$field];

		    	return ($item->$field == $value || $item->$afield == $value);
		    }

		    return $item->$field == $value;
		
		}) !== false;
    }
}