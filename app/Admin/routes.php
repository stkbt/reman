<?php

Route::get('duplicates', ['as' => 'admin.duplicates', function () {

	$ps = \App\Product::orderBy('created_at')->get()
		->groupBy('hash')
		->filter(function($item){ return $item->count() > 1; })
		;


	dump('Число дубликатов: ' . $ps->count());

	if(request('delete')) {

		foreach ($ps as $key => $value) {
			
			$value->pop();

			foreach ($value as $p) {
				$p->delete();
			}
		}
	}



	dd();

	return AdminSection::view($content, 'Dashboard');
}]);

/* Import */

Route::get('import', ['as' => 'admin.import', 'uses' => 'App\Admin\Controllers\ImportController@index']);
Route::get('import/submit', ['as' => 'admin.import.submit', 'uses' => 'App\Admin\Controllers\ImportController@submitImport']);
Route::get('import/run', ['as' => 'admin.import.run', 'uses' => 'App\Admin\Controllers\ImportController@sendRunImport']);


Route::post('createBill', ['as' => 'admin.createbill', 'uses' => 'App\Admin\Controllers\BillController@createBill']);
Route::post('addToBill', ['as' => 'admin.addToBill', 'uses' => 'App\Admin\Controllers\BillController@addToBill']);

Route::get('bills/{id}/download', ['as' => 'admin.bills.download', 'uses' => 'App\Admin\Controllers\BillController@downloadBill']);

Route::get('billData', ['as' => 'admin.bill.data', 'uses' => 'App\Admin\Controllers\BillController@billData']);
Route::get('billitemData', ['as' => 'admin.billitems.data', 'uses' => 'App\Admin\Controllers\BillController@billitemData']);

Route::post('searchSubmitByTitle', ['as' => 'admin.searchSubmitByTitle', 'uses' => 'App\Admin\Controllers\SearchController@searchSubmitByTitle']);

Route::post('searchSubmit', ['as' => 'admin.searchSubmit', 'uses' => 'App\Admin\Controllers\SearchController@searchSubmit']);


Route::post('searchHistorySubmit/{id}', ['as' => 'admin.searchHistorySubmit', 'uses' => 'App\Admin\Controllers\SearchController@searchHistorySubmit']);
Route::get('searchHistoryView/{search_history}', ['as' => 'admin.searchHistoryView', 'uses' => 'App\Admin\Controllers\SearchController@searchHistoryView']);

Route::any('searchHistorySendVed/{search_history}', ['as' => 'admin.searchHistorySendVed', 'uses' => 'App\Admin\Controllers\SearchController@searchHistorySendVed']);

Route::get('searchHistoryDownload/{search_history}', ['as' => 'admin.searchHistoryDownload', 'uses' => 'App\Admin\Controllers\SearchController@searchHistoryDownload']);


Route::post('updateImportProducts', ['as' => 'admin.updateImportProducts', 'uses' => 'App\Admin\Controllers\ImportController@updateImportProducts']);
Route::post('removeImportProducts', ['as' => 'admin.removeImportProducts', 'uses' => 'App\Admin\Controllers\ImportController@removeImportProducts']);


Route::post('import_products/create', ['as' => 'admin.updateImportProducts.all', 'uses' => 'App\Admin\Controllers\ImportController@updateImportProducts']);


Route::get('reverseImport/{id}', ['as' => 'admin.reverseImport', 'uses' => 'App\Admin\Controllers\ImportController@reverseImport']);


Route::get('updatecurrencies', ['as' => 'admin.updatecurrencies', 'uses' => function(){

	\Artisan::call('updatecurrencies');

	return redirect()->back();
}]);


Route::post('products', [
    'as'   => 'admin.products.post',
    'uses' => 'AdminController@getDisplay',
]);

Route::post('searchAjax', ['as' => 'admin.searchAjax', 'uses' => 'App\Admin\Controllers\SearchController@searchAjax']);
