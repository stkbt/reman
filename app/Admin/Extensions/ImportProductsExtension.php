<?php

namespace App\Admin\Extensions;

use SleepingOwl\Admin\Contracts\Display\Placable;
use SleepingOwl\Admin\Display\Extension\Actions;

class ImportProductsExtension extends Actions
{
    
    protected $placement = 'panel.heading.actions';

    protected $view = 'display.extensions.import_products_extension';
}