<?php

namespace SleepingOwl\Admin\Display\Column;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\RepositoryInterface;
use SleepingOwl\Admin\Exceptions\Form\Element\SelectException;

class AdminColumnActionSelect extends Action {


	protected $view = 'column.actionselect';

	protected $options = [];
	protected $foreignKey = null;
	protected $modelForOptions;
	protected $display = 'title';
	protected $fetchColumns = [];
	protected $loadOptionsQueryPreparer = null;

	protected $defaultValue = null;

	protected $select_name = null;
	protected $select_value = null;

	protected $pretitle = null;


	public function initialize()
    {
        $this->setHtmlAttributes([
            'class' => 'btn btn-action btn-warning',
            'name'  => 'action',
            'value' => $this->getName(),
            'data-action' => $this->getAction(),
            'data-method' => $this->getMethod()
        ]);


    }

    public function toArray()
    {

    	$options = $this->getOptions();

        $options = [null => trans('sleeping_owl::lang.select.nothing')] + $options;

        return parent::toArray() + [
            'icon'   => $this->getIcon(),
            'action' => $this->getAction(),
            'method' => $this->getMethod(),
            'title'  => $this->getTitle(),
            'pretitle'  => $this->getPreTitle(),
            'select_name' => $this->getSelectName(),
            'select_value' => $this->getSelectValue(),
            'options' => $options,
        ];
    }


    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function setSelectName($value) {

    	$this->select_name = $value;

    	return $this;
    }
    public function getSelectName() {

    	return $this->select_name;
    }
    public function setSelectValue($value) {

    	$this->select_value = $value;

    	return $this;
    }
    public function getSelectValue() {

    	return $this->select_value;
    }

    public function setPreTitle($value) {

    	$this->pretitle = $value;

    	return $this;
    }
    public function getPreTitle() {

    	return $this->pretitle;
    }


	public function isEmptyRelation()
    {
        return false;
    }

    public function getOptions()
    {
        if (! is_null($this->getModelForOptions()) && ! is_null($this->getDisplay())) {
            $this->loadOptions();
        }

        $options = $this->options;
        // if ($this->isSortable()) {
        //     asort($options);
        // }

        return $options;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function getModelForOptions()
    {
        return $this->modelForOptions;
    }

    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

	public function setModelForOptions($modelForOptions)
    {
        if (is_string($modelForOptions)) {
            $modelForOptions = app($modelForOptions);
        }

        if (! ($modelForOptions instanceof Model)) {
            throw new SelectException('Class must be instanced of Illuminate\Database\Eloquent\Model');
        }

        $this->modelForOptions = $modelForOptions;

        return $this;
    }

    public function getForeignKey()
    {
        if (is_null($this->foreignKey)) {
            return $this->foreignKey = $this->getModel()->getForeignKey();
        }

        return $this->foreignKey;
    }

    public function setFetchColumns($columns)
    {
        if (! is_array($columns)) {
            $columns = func_get_args();
        }

        $this->fetchColumns = $columns;

        return $this;
    }

    public function getFetchColumns()
    {
        return $this->fetchColumns;
    }

    protected function loadOptions()
    {
        $repository = app(RepositoryInterface::class, [$this->getModelForOptions()]);

        $key = $repository->getModel()->getKeyName();

        $options = $repository->getQuery();

        // if ($this->isEmptyRelation()) {
        //     $options->where($this->getForeignKey(), 0)->orWhereNull($this->getForeignKey());
        // }

        if (count($this->fetchColumns) > 0) {
            $columns = array_merge([$key], $this->fetchColumns);
            $options->select($columns);
        }

        // call the pre load options query preparer if has be set
        if (! is_null($preparer = $this->getLoadOptionsQueryPreparer())) {
            $options = $preparer($this, $options);
        }

        $options = $options->get();

        if (is_callable($this->getDisplay())) {

            // make dynamic display text
            if ($options instanceof Collection) {
                $options = $options->all();
            }

            // the maker
            $makeDisplay = $this->getDisplay();

            // iterate for all options and redefine it as
            // list of KEY and TEXT pair
            $options = array_map(function ($opt) use ($key, $makeDisplay) {
                // get the KEY and make the display text
                return [data_get($opt, $key), $makeDisplay($opt)];
            }, $options);

            // take options as array with KEY => VALUE pair
            $options = Arr::pluck($options, 1, 0);
        } elseif ($options instanceof Collection) {
            // take options as array with KEY => VALUE pair
            $options = Arr::pluck($options->all(), $this->getDisplay(), $key);
        } else {
            // take options as array with KEY => VALUE pair
            $options = Arr::pluck($options, $this->getDisplay(), $key);
        }

        $this->setOptions($options);
    }


    public function setLoadOptionsQueryPreparer($callback)
    {
        $this->loadOptionsQueryPreparer = $callback;

        return $this;
    }

    public function getLoadOptionsQueryPreparer()
    {
        return $this->loadOptionsQueryPreparer;
    }

}