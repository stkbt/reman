<?php

namespace SleepingOwl\Admin\Display\Column\Editable;

use SleepingOwl\Admin\Form\FormDefault;
use SleepingOwl\Admin\Display\Column\NamedColumn;
use SleepingOwl\Admin\Contracts\Display\ColumnEditableInterface;

class AdminColumnEditableText extends NamedColumn implements ColumnEditableInterface
{
    /**
     * @var string
     */
    protected $view = 'column.editable.columneditabletext';

    /**
     * @var null|string
     */
    protected $value = null;


    /**
     * Checkbox constructor.
     *
     * @param             $name
     */
    public function __construct($name, $label = null)
    {
        parent::__construct($name);
        $this->setLabel($label);
        
        
    }


    /**
     * @return array
     */
    public function toArray()
    {   
        
        return parent::toArray() + [
            'id'             => $this->getModel()->getKey(),
            'value'          => $this->getModelValue(),
            'isEditable'     => $this->getModelConfiguration()->isEditable($this->getModel()),
        ];
    }

    /**
     * Save form item.
     *
     * @param mixed $value
     */
    public function save(\Illuminate\Http\Request $request)
    {
        // if (is_array($value)) {
        //     $value = array_shift($value);
        // }

        $this->getModel()->setAttribute($this->getName(), $request->input('value'));
        $this->getModel()->save();

        // $form = new FormDefault([
        //     new \SleepingOwl\Admin\Form\Element\Text(
        //         $this->getName()
        //     ),
        // ]);

        // $model = $this->getModel();

        // $request->offsetSet($this->getName(), $request->input('value'));

        // $form->setModel($model);
        // $form->saveForm($request);
    }
}
