<?php

namespace App\Admin\Controllers;

use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Model\ModelConfiguration;

use DB;
use Request;

use AdminSection;
use AdminForm;
use AdminFormElement;

use Excel;

use App\Bill;
use App\Billitem;
use App\Product;


class BillController extends AdminController
{

    public function createBill() {

        $response = ['success' => false];

        $data = request('data', []);
        $data = collect($data)->keyBy('id');

        $product_ids = $data->keys()->all();

        $products = Product::whereIn('id', $product_ids)
            ->orderByRaw(DB::raw("FIELD(id, ".implode(',', $product_ids).")"))
            ->get();

        $products->map(function($item, $key) use ($data){

            $item->count = array_get($data->get($item->id), 'count', 1);
            return $item;
        });

        $bill = Bill::create([
            'is_draft' => 1,
            'items' => $products,
            'currency_id' => 1,
            'search_history_id' => request('search_history_id'),
            'contractor_id' => request('contractor_id')
        ]);

        if($bill) {
            $response['success'] = true;
            $response['bill_id'] = $bill->id;
        }


        return response()->json($response);
    }

    public function addToBill() {

    	$response = ['success' => false];

        $data = request('data', []);
        $data = collect($data)->keyBy('id');

        $product_ids = $data->keys()->all();

        $products = Product::whereIn('id', $product_ids)->get();

        $products->map(function($item, $key) use ($data){

            $item->count = array_get($data->get($item->id), 'count', 1);
            return $item;
        });

        $bill = Bill::find(request('bill_id'));

        if($bill) {

            $bill->addItems($products);

        	$response['success'] = true;
        	$response['bill_id'] = $bill->id;
        }


        return response()->json($response);
    }

    public function downloadBill($id) {

    	$format = request('format', 'xls');

    	$bill = Bill::find($id);

    	if($bill) {

            $bill->generateFileExport($format, 'download');
    	}

    	return redirect()->back();

    }


    public function billData() {

        $id = request('id');
        $fields = request('fields');

        $bill= Bill::find($id);

        if($bill) {

            if($fields) {

                foreach ($fields as $field => $value) {
                    $bill->$field = $value;
                }

                $bill->save();
            }


            return response()->json([
                'total_rub_formatted' => $bill->total_rub_formatted,
                'total_cur_formatted' => $bill->total_cur_formatted,
                'currency_title' => $bill->currency->title,
                'with_nds_title' => $bill->with_nds_title,
                'fields' => [
                    'delivery_time' => $bill->delivery_time,
                    'paymentmethod' => $bill->paymentmethod,
                    'delivery_conditions' => $bill->delivery_conditions,
                    'notes' => $bill->notes,
                    // 'organization_id' => $bill->organization_id
                ]
            ]);
        }


        return response()->json([]);
    }

    public function billitemData() {

        $id = request('id');

        $billitem = Billitem::find($id);

        if($billitem) {

            return response()->json([
                'total_rub_formatted' => $billitem->total_rub_formatted
            ]);
        }


        return response()->json([]);
    }

}