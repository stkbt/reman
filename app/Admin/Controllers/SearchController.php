<?php

namespace App\Admin\Controllers;

use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Model\ModelConfiguration;

use DB;
use Request;

use AdminSection;
use AdminForm;
use AdminFormElement;
use AdminDisplay;
use AdminColumn;

use App\Product;
use App\SearchHistory;
use App\AddRequest;


class SearchController extends AdminController
{
    public function searchSubmitByTitle() {

        $title = request('title');
        
        $brand_id = request('brand_id', []);

        $bill_id = request('bill_id');

        $filters = [
            'title' => $title,
            'brand_id' => $brand_id,
            'is_search' => 1
        ];

        if($bill_id) $filters['bill_id'] = $bill_id;


        $link = url('products?' . http_build_query($filters));


        return redirect($link)->withInput($filters);
    }


    public function searchSubmit() {

        $skus_quantity = preg_split( "/\r\n/", request('skus'));

        $skus = collect($skus_quantity)
            ->map(function($item, $key){

                $item_arr = preg_split( "/(\s)/", $item);

                if(count($item_arr) > 1) {
                    $count = array_pop($item_arr);

                    if(!$count) {
                        $count = 1;
                    }
                }
                else {
                    $count = 1;
                }

                $item_sku = strtoupper(implode('', $item_arr));

                $item_sku = str_replace(['Н', 'Р'], ['H', 'P'], $item_sku);

                $item_sku = str_replace(' ', '', $item_sku);

                $item_sku = str_replace('-', '', str_slug($item_sku));


                return ['sku' => strtoupper((string)$item_sku), 'count' => (int)$count];
            })
            ->filter(function($item, $key){

                return !empty($item['sku']);
            })
        ;

        // dd($skus[0]['sku'] == 'R23');

        // \MessagesStack::addInfo('Вы искали артикулы: ' . implode('|', $skus));

        $bill_id = request('bill_id');
        
        $brand_id = request('brand_id', []);

        $search_alternate = request('search_alternate', 0);

        $history_title = request('history_title');

        $contractor_id = request('contractor_id');

        
        $create_sh = self::createSearchHistory($skus, $brand_id, $search_alternate, $history_title, $contractor_id);
        
        $ids = $create_sh['product_ids'];
        $search_history_id = $create_sh['search_history_id'];

        $filters = [
            'sku' => $skus->pluck('sku')->all(),
            'count' => $skus->pluck('count')->all(),
            'bill_id' => $bill_id,
            'search_history_id' => $search_history_id,
            'contractor_id' => $contractor_id,
            'brand_id' => $brand_id,
            'search_alternate' => $search_alternate,
            'is_search' => 1
        ];


        if($bill_id) $filters['bill_id'] = $bill_id;

        // $link = url('products?' . http_build_query($filters));


        $filters['data'] = [
            'delivery_time_max' => DB::table('products')->whereIn('id', $ids)->max('delivery_time_max'),
            'brands' => DB::table('products')->whereIn('id', $ids)->pluck('brand_id'),
            'countries' => DB::table('products')->whereIn('products.id', $ids)->join('brands', 'brands.id', '=', 'products.brand_id')->pluck('brands.country_id'),
        ];


        return redirect()->route('admin.products.post', ['search' => 1, 'search_history_id' => $search_history_id, 'contractor_id' => $contractor_id])->withInput($filters);
    }


    public function searchAjax() {

        $sku = request('sku');

        $sku = strtoupper((string)$sku);
        $sku = str_replace(['Н', 'Р'], ['H', 'P'], $sku);
        $sku = str_replace('-', '', str_slug($sku));

        $product = null;

        if($sku && !empty(trim($sku))) {
            $product = Product::where('sku', $sku)->orWhere('sku_alternate', $sku)->first();
        }


        if($product) {

            return response()->json([
                'success' => true,
                'item' => $product,
                'sku' => $sku
            ]);
        }
        else {

            return response()->json([
                'success' => false,
                'sku' => $sku
            ]);
        }
    }


    public function searchHistorySubmit($id) {

        $searchHistory = SearchHistory::find($id);

        if(!$searchHistory) {

            \MessagesStack::addDanger('Элемент истории поиска не найден #' . $id);

            return redirect('/');
        }


        $skus = collect($searchHistory->sku);
        $brand_id = $searchHistory->brand_id;
        $search_alternate = $searchHistory->search_alternate;


        // SearchHistory::create([
        //     'sku' => $skus,
        //     'brand_id' => $brand_id,
        //     'search_alternate' => $search_alternate 
        // ]);


        $ids = self::getProducts($skus->keys()->all(), $brand_id, $search_alternate)->pluck('id');


        $filters = [
            'sku' => $skus->pluck('sku')->all(),
            'count' => $skus->pluck('count')->all(),
            'brand_id' => $brand_id,
            'search_alternate' => $search_alternate
        ];

       


        $filters['data'] = [
            'delivery_time_max' => DB::table('products')->whereIn('id', $ids)->max('delivery_time_max'),
            'brands' => DB::table('products')->whereIn('id', $ids)->pluck('brand_id'),
            'countries' => DB::table('products')->whereIn('products.id', $ids)->join('brands', 'brands.id', '=', 'products.brand_id')->pluck('brands.country_id'),
        ];


       

        return redirect()->route('admin.products.post', ['search' => 1, 'search_history_id' => $id, 'contractor_id' => $searchHistory->contractor_id])->withInput($filters);
    }

    
    public function searchHistoryView(SearchHistory $search_history) {
        

        return self::getSearchHistoryView($search_history);

        // $results = $search_history->result;

        // $results_skus = collect($results)->map(function($item){

        //     return $item['sku'];

        // })->toArray();

        // $not_found_skus = collect($search_history->sku)->keys()
        //     ->filter(function($item) use ($results_skus){

        //         return !in_array($item, $results_skus);
        //     });



        // return view('admin.sh_results', [
        //     'items' => $results,
        //     'not_found_skus' => $not_found_skus,
        //     'search_history' => $search_history
        // ]);

    }

    public static function getSearchHistoryView(SearchHistory $search_history) {

        $results = $search_history->result;

        $results_skus = collect($results)->map(function($item){

            return $item['sku'];

        })->toArray();

        $not_found_skus = collect($search_history->sku)->keys()
            ->filter(function($item) use ($results_skus){

                return !in_array($item, $results_skus);
            });



        return view('admin.sh_results', [
            'items' => $results,
            'not_found_skus' => $not_found_skus,
            'search_history' => $search_history
        ]);

    }


    private static function createSearchHistory($skus, $brands, $search_alternate, $history_title = null, $contractor_id = null) {

        $skus = $skus->transform(function ($item, $key) {
            
            $item['sku'] = strtoupper($item['sku']);

            return $item;
        })
        ->keyBy('sku')
        ;


        $products = self::getProducts($skus->keys()->all(), $brands, $search_alternate);
        

        $products->map(function($item, $key) use ($skus){

            $item->count = array_get($skus->get($item->sku), 'count', 1);

            return $item;
        });

        $total = $products->sum(function($product) {
            
            return $product->priceRubValue * $product->count;
        });

        $weight = $products->sum(function($product) {
            
            return $product->weight * $product->count;
        });

        $result = collect();

        foreach($products as $product) {

            $result->push([
                'id' => $product->id,
                'sku' => $product->sku,
                'sku_alternate' => $product->sku_alternate,
                'title' => $product->title,
                'title_en' => $product->title_en,
                'price' => $product->price_formatted,
                'currency_title' => $product->currency ? $product->currency->title : '',
                'price_rub' => $product->price_rub,
                'delivery_time' => $product->delivery_time,
                'weight' => $product->weight,
                'brand_title' => $product->brand ? $product->brand->title : '',
                'count' => $product->count
            ]);
        }


        $results_skus = collect($result)->map(function($item){

            return $item['sku'];

        })->toArray();

        $not_found_skus = $skus->keys()
            ->filter(function($item) use ($results_skus){

                return !in_array($item, $results_skus);
            });

        $status = $not_found_skus->count() == 0 ? 0 : 1;


        $search_history = SearchHistory::create([
            'sku_raw' => json_encode($skus),
            'brand_id' => $brands,
            'search_alternate' => $search_alternate,
            'total' => $total,
            'weight' => $weight,
            'result' => $result,
            'title'  => $history_title != '' ? $history_title : null,
            'status' => $status,
            'contractor_id' => $contractor_id
        ]);


        return [
                'product_ids' => $products->pluck('id'),
                'search_history_id' => $search_history
            ];
    }



    public function searchHistorySendVed(SearchHistory $search_history) {

        $search_history->status = 2;
        $search_history->save();


        $addrequest = AddRequest::create([
            'search_history_id' => $search_history->id,
            'sender_message' => request('sender_message')
        ]);


        return response()->json([
            'success' => !is_null($addrequest)
        ]);


        // \MessagesStack::addSuccess('Заявка на добавление позиций успешно отправлена в отдел ВЭД');

        // return redirect()->back();
    }

    public function searchHistoryDownload(SearchHistory $search_history) {

        
        return $search_history->generateFileExport();
    }




    public static function getProducts($skus, $brands, $search_alternate) {

        $products = null;


        if($search_alternate) {

            $products = Product::where(function($query) use ($skus, $search_alternate){

                $query
                        ->whereIn('products.sku', $skus)
                        ->orWhereIn('products.sku_alternate', $skus)
                        ;
            });
        }
        else {

            $products = Product::whereIn('products.sku', $skus);
        }

        if($brands && count($brands)) {

            $products->whereIn('products.brand_id', $brands);
        }


        // if(count($skus)) {
        //     $products->orderByRaw(DB::raw("FIELD(sku, ".implode(',', $skus).")"));
        // }


        return $products->get();
    }


}