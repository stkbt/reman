<?php

namespace App\Admin\Controllers;

use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Model\ModelConfiguration;

use DB;
use Request;
use Carbon\Carbon;

use AdminSection;
use AdminForm;
use AdminFormElement;

use Excel;

use App\Product;
use App\ImportProduct;
use App\Brand;
use App\Supplier;
use App\Import;

use MessagesStack;


class ImportController extends AdminController
{

    protected $currencies = [
        'RUR' => 1,
        'USD' => 2,
        'EUR' => 3,
    ];

    protected $brands = null;

    protected $periods = [
        'month'  => ['месяцев', 'мес.', 'мес'],
        'week'  => ['недель', 'нед.', 'нед'],
        'day'   => ['дней', 'дн.', 'дн']
    ];


    protected $fields = [
        'sku', 'sku_alternate', 'title', 'title_en', 'price', 'currency_id', 'delivery_time_min', 'delivery_time_max', 'weight', 'brand_id', 'import_id', 'quantity'
    ];

    
    // public function index() {
        
    //     $importPanel = AdminForm::panel();

    //     $importPanel->addBody([
    //     	AdminFormElement::text('sku', 'Артикул (внутренний)')
    //     ]);

    //     $importPanel->setAction(route('admin.import.submit'));
    //     $importPanel->setModel(new Import);
    //     $importPanel->setModelClass(Import::class);

    //     // dump($importPanel);
        
    //     $view = view('admin.import', compact('importPanel'));
        
    //     return AdminSection::view($view, 'Импорт информации');
    // }

    public function updateImportProducts() {
        
        $response = ['success' => false];

        $imp_products = ImportProduct::with('duplicate');

        if(!request('all')) {

            $data = request('data', []);
            $data = collect($data)->keyBy('id');

            $imp_product_ids = $data->keys()->all();

            $imp_products->whereIn('id', $imp_product_ids);
        }

        $imp_products->where('import_id', request('import_id'));

        $imp_products = $imp_products->get();

        foreach ($imp_products as $imp_product) {
            
            if($imp_product->duplicate) {

                foreach ($this->fields as $field) {
                    
                    $imp_product->duplicate->$field = $imp_product->$field;
                }

                $imp_product->duplicate->priced_at = Carbon::now()->toDateTimeString();
                $imp_product->duplicate->save();
            }
            else {

                $product = Product::create(collect($imp_product->toArray())->only($this->fields)->all());

                $product->priced_at = Carbon::now()->toDateTimeString();
                $product->save();
            }

            

            $imp_product->is_imported = 1;
            $imp_product->save();

        }


        $response['success'] = true;

        return response()->json($response);
    }

    public function removeImportProducts() {

        $response = ['success' => false];

        $data = request('data', []);
        $data = collect($data)->keyBy('id');

        $imp_product_ids = $data->keys()->all();

        $imp_products = ImportProduct::whereIn('id', $imp_product_ids)->get();

        foreach ($imp_products as $imp_product) {
            
            $imp_product->delete();
        }

        $response['success'] = true;

        return response()->json($response);
    }



    public function reverseImport(Import $import) {

        foreach ($import->import_products->where('is_imported', 1) as $imp_product) {
            
            $prev_imp_product = ImportProduct::where('hash', $imp_product->hash)->where('import_id', '<', $import->id)->orderBy('import_id', 'desc')->first();

            if($prev_imp_product) {

                if($prev_imp_product->duplicate) {

                    foreach ($this->fields as $field) {
                        
                        $prev_imp_product->duplicate->$field = $prev_imp_product->$field;
                    }

                    $prev_imp_product->duplicate->priced_at = Carbon::now()->toDateTimeString();
                    $prev_imp_product->duplicate->save();
                }
            }
            else {

                if($imp_product->duplicate) {

                    $imp_product->duplicate->delete();
                }
            }

            $imp_product->delete();
        }

        MessagesStack::addInfo('<i class="fa fa-check"></i> Удаление импорта завершено. Товары возвращены в состояние предыдущего импорта');

        // $import->delete();
    }


    // public function sendRunImport() {

    //     $filename = request('filename');
    //     $import_id = request('import_id');

    //     $this->runImport($filename, $import_id);

    // }

    // public function runImport($filename, $import_id = 0) {

    //     $filename = public_path($filename);

    //     $data = Excel::load($filename, function($reader) {

            

    //     })->all()->get(0);

    //     $icount = 0;

        

    //     foreach($data as $row) {

    //         $values = $row->values();

    //         $delivery_range = $this->getDeliveryRange($values[7]);

    //         $product = ImportProduct::create([
    //             'sku' => (string)$values[0],
    //             'sku_alternate' => (string)$values[1],
    //             'title' => $values[2],
    //             'title_en' => $values[3],
    //             'weight' => $values[4],
    //             'price' => $values[5],
    //             'currency_id' => $this->getCurrency($values[6]),
    //             'delivery_time_min' => $delivery_range['min'],
    //             'delivery_time_max' => $delivery_range['max'],
    //             'brand_id' => $this->getBrand($values[8]),
    //             'supplier_id' => 1,
    //             'import_id' => $import_id,
    //             'priced_at' => Carbon::now()
    //         ]);

    //         if($product) $icount++;
    //     }


    //     dd($icount);
    // }


    // private function getCurrency($_symbol) {

    //     if(isset($this->currencies[$_symbol])) return $this->currencies[$_symbol];

    //     return 1;
    // }

    // private function getBrand($_title) {

    //     if(!$this->brands) $this->brands = Brand::pluck('id', 'title');

    //     if(isset($this->brands[$_title])) return $this->brands[$_title];

    //     $brand = Brand::create([
    //         'title' => $_title
    //     ]);

    //     $this->brands[$brand->title] = $brand->id;

    //     return $brand->id;
    // }

    // private function getDeliveryRange($_value) {
        
    //     $val = null;

    //     $_value = trim($_value);

    //     $period = null;

    //     foreach($this->periods as $tperiod => $tvars) {

    //         foreach($tvars as $tvar) {

    //             if(strstr($_value, $tvar)) {

    //                 $period = $tperiod;

    //                 $_value = str_replace($tvar, '', $_value);
    //                 $_value = trim($_value);
    //             }
    //         }
    //     }

    //     $time_unit = 0;
    //     switch($period) {
    //         case 'month':
    //         $time_unit = 60*60*24*30;
    //         break;
    //         case 'week':
    //         $time_unit = 60*60*24*7;
    //         break;
    //         case 'day':
    //         $time_unit = 60*60*24;
    //         break;

    //     }

    //     $values = explode('-', $_value);

    //     $min = $values[0] * $time_unit;

    //     $max = count($values) == 2 ? $values[1] * $time_unit : $min;

    //     return [
    //         'min' => $min,
    //         'max' => $max
    //     ];
        
    // }

    
    
}