<?php

namespace SleepingOwl\Admin\Form;

use SleepingOwl\Admin\Form\FormButtons;
use SleepingOwl\Admin\Contracts\FormButtonsInterface;

class FormButtonsEmail extends FormButtons implements FormButtonsInterface
{

	protected $view = 'form.buttonsemail';

	protected $backUrl = null;

	public function setBackUrl($value) {

		$this->backUrl = $value;
	}
	public function getBackUrl() {

		if($this->backUrl) return $this->backUrl;

		return $this->getModelConfiguration()->getDisplayUrl();
	}

	public function toArray()
    {
        return [
            'attributes'              => $this->htmlAttributesToString(),
            'backUrl'                 => $this->getBackUrl(),
            'editUrl'                 => $this->getModelConfiguration()->getEditUrl($this->getModel()->getKey()),
            'deleteUrl'               => $this->getModelConfiguration()->getDeleteUrl($this->getModel()->getKey()),
            'destroyUrl'              => $this->getModelConfiguration()->getDestroyUrl($this->getModel()->getKey()),
            'restoreUrl'              => $this->getModelConfiguration()->getRestoreUrl($this->getModel()->getKey()),
            'saveButtonText'          => $this->getSaveButtonText(),
            'saveAndCloseButtonText'  => $this->getSaveAndCloseButtonText(),
            'saveAndCreateButtonText' => $this->getSaveAndCreateButtonText(),
            'cancelButtonText'        => $this->getCancelButtonText(),
            'deleteButtonText'        => $this->getDeleteButtonText(),
            'destroyButtonText'       => $this->getDestroyButtonText(),
            'restoreButtonText'       => $this->getRestoreButtonText(),
            'showCancelButton'        => $this->isShowCancelButton(),
            'showSaveAndCloseButton'  => $this->isShowSaveAndCloseButton(),
            'showSaveAndCreateButton' => $this->isShowSaveAndCreateButton(),
            'showDeleteButton'        => $this->isShowDeleteButton(),
            'showDestroyButton'       => $this->isShowDestroyButton(),
            'showRestoreButton'       => $this->isShowRestoreButton(),
        ];
    }
}