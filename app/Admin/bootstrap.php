<?php

// PackageManager::load('admin-default')
//    ->css('extend', resources_url('css/extend.css'));


app('sleeping_owl.table.column.editable')->add('text', SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText::class);

app('sleeping_owl.table.column')->add('actionselect', SleepingOwl\Admin\Display\Column\AdminColumnActionSelect::class);

app('sleeping_owl.table.column')->add('text_duplicate', SleepingOwl\Admin\Display\Column\AdminColumnTextDuplicate::class);

app('sleeping_owl.display')->add('datatebles_withnotfound', SleepingOwl\Admin\Display\DatatablesWithNotFound::class);

app('sleeping_owl.display')->add('datatablesAsyncExteneded', SleepingOwl\Admin\Display\DatatablesAsyncExtended::class);

app('sleeping_owl.form')->add('formButtonsEmail', SleepingOwl\Admin\Form\FormButtonsEmail::class);