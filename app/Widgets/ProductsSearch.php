<?php

namespace App\Widgets;

use DB;

use SleepingOwl\Admin\Widgets\Widget;
use AdminFormElement;

class ProductsSearch extends Widget
{

    public function active()
    {
        return request()->is('products') && request('search');
    }

    /**
     * HTML который необходимо поместить
     *
     * @return string
     */
    public function toHtml()
    {
        $bill_id = old('bill_id', request('bill_id'));

        $brandsSelect = AdminFormElement::multiselect('brand', 'Фильтр по производителям')
            ->setModelForOptions('App\Brand')
            ->setLoadOptionsQueryPreparer(function($element, $query) {
                $query
                    ->whereNotNull('title');

                if(old('data.brands')) {

                    $query->whereIn('id', old('data.brands'));
                }

                return $query;
            })
            ;



        $countriesSelect = AdminFormElement::multiselect('country', 'Фильтр по стране')
            ->setModelForOptions('App\Country')
            ->setLoadOptionsQueryPreparer(function($element, $query) {
                $query
                    ->whereNotNull('title');

                if(old('data.countries')) {

                    $query->whereIn('id', old('data.countries'));
                }

                return $query;
            })
            ;


        $delivery_times_minmax = [DB::table('products')->min('delivery_time_max')/86400, DB::table('products')->max('delivery_time_max')/86400];

        if(old('data.delivery_time_max')) {
            $delivery_times_minmax[1] = old('data.delivery_time_max')/86400;
        }


        return view('admin.productssearch', compact('bill_id', 'brandsSelect', 'countriesSelect', 'delivery_times_minmax'))->render();
    }

    /**
     * Путь до шаблона, в который добавляем
     *
     * @return string|array
     */
    public function template()
    {
        // AdminTemplate::getViewPath('dashboard') == 'sleepingowl:default.dashboard'
        return \AdminTemplate::getViewPath('_layout.inner');
    }

    /**
     * Блок в шаблоне, куда помещаем
     *
     * @return string
     */
    public function block()
    {
        return 'content.top';
    }
}