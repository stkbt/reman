<?php

namespace App\Widgets;

use DB;

use SleepingOwl\Admin\Widgets\Widget;
use AdminFormElement;

use App\Bill;
use App\SearchHistory;

class BillsFilters extends Widget
{

    public function active()
    {
        return request()->is('bills') || request()->is('search_histories');
    }

    /**
     * HTML который необходимо поместить
     *
     * @return string
     */
    public function toHtml()
    {

        $contractorSelect = AdminFormElement::select('contractor_id', 'Фильтр по контрагенту')
            ->setModelForOptions('App\Contractor')
            ->setLoadOptionsQueryPreparer(function($element, $query) {
                $query
                    ->whereNotNull('title');

                return $query;
            })
            ->nullable()
            ;

        $userSelect = null;

        if(\Auth::user()->user_status > 0) {
            $userSelect = AdminFormElement::select('user_id', 'Фильтр по менеджеру')
                ->setModelForOptions('App\User')
                ->setDisplay('name')
                ->nullable()
                ;
        }


        $organizationSelect = null;

        if(request()->is('bills')) {
            
            $organizationSelect = AdminFormElement::select('organization_id', 'Фильтр по организации')
                ->setModelForOptions('App\Organization')
                ->setDisplay('name')
                ->nullable()
                ;
        }





        $description = null;

        $model = null;

        if(request()->is('bills')) {
            $model = Bill::orderBy('id')->with('items');
        }
        else if(request()->is('search_histories')) {
            $model = SearchHistory::orderBy('id');
        }

        if(request('contractor_id')) {
            $model->where('contractor_id', request('contractor_id'));
        }
        if(request('user_id')) {
            $model->where('user_id', request('user_id'));
        }
        if(request('organization_id')) {
            $model->where('organization_id', request('organization_id'));
        }



        if(request()->is('bills')) {
            $sum = $model->get()->sum('total_rub');
        }
        else if(request()->is('search_histories')) {
            $sum = $model->sum('total');
        }


        
        $sum = number_format($sum, 2, ',', ' ');

        $count = $model->count();

        if(request('user_id') || request('contractor_id')) {

            $description = "Найдено <b>$count</b> поз., на сумму <b>$sum</b>";
        }


        return view('admin.billsfilters', compact('contractorSelect', 'userSelect', 'description', 'organizationSelect'))->render();
    }

    /**
     * Путь до шаблона, в который добавляем
     *
     * @return string|array
     */
    public function template()
    {
        // AdminTemplate::getViewPath('dashboard') == 'sleepingowl:default.dashboard'
        return \AdminTemplate::getViewPath('_layout.inner');
    }

    /**
     * Блок в шаблоне, куда помещаем
     *
     * @return string
     */
    public function block()
    {
        return 'content.top';
    }
}