<?php

namespace App\Widgets;

use SleepingOwl\Admin\Widgets\Widget;
use AdminFormElement;

use App\Brand;
use App\SearchHistory;
use App\Currency;

class DashboardSearch extends Widget
{

    /**
     * HTML который необходимо поместить
     *
     * @return string
     */
    public function toHtml()
    {
        $brandsSelect = AdminFormElement::multiselect('brand_id', 'Фильтр по производителям')
            ->setModelForOptions('App\Brand')
            ->setLoadOptionsQueryPreparer(function($element, $query) {
                return $query
                    ->whereNotNull('title');
            })
            ;

        $contractorSelect = AdminFormElement::select('contractor_id', 'Заявка от контрагента')
            ->setModelForOptions('App\Contractor')
            ->setLoadOptionsQueryPreparer(function($element, $query) {
                return $query
                    ->where('title', '!=', "")
                    ->whereNotNull('title');
            })
            ->setDisplay('title_inn')
            ->setHelpText('Доступен поиск по названию или ИНН')
            ;

        // $searchHistories = SearchHistory::orderBy('created_at', 'desc')->get();

        $currencies = Currency::where('id', '<>', 1)->get();


        return view('admin.dashboardsearch', compact('brandsSelect', 'currencies', 'contractorSelect'))->render();
    }

    /**
     * Путь до шаблона, в который добавляем
     *
     * @return string|array
     */
    public function template()
    {
        // AdminTemplate::getViewPath('dashboard') == 'sleepingowl:default.dashboard'
        return \AdminTemplate::getViewPath('dashboard');
    }

    /**
     * Блок в шаблоне, куда помещаем
     *
     * @return string
     */
    public function block()
    {
        return 'block.content';
    }
}