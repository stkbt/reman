<?php

namespace App\Listeners;

use Log;
use Mail;
use App\Events\OrderPlaced;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class EmailsOrderPlaced
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlaced  $event
     * @return void
     */
    public function handle(OrderPlaced $event)
    {
        $order = $event->order;
        $user = $event->user;
        
        
        Log::info('EmailsOrderPlaced handle: $order->id = ' . $order->id);
        
        //to buyer
        if($user){
            
            $data = [
                'theme'     => 'Ваш заказ',
                'email'     => $user->email
            ];
            $sent = Mail::send('mail.order_touser', compact('order'), function($m) use ($data) {
                
                $m->to($data['email'])->subject($data['theme']);
            });
            if($sent === 0) {
                Log::emergency('Ошибка отправки письма с заказом покупателю: $order => ' . $order->id);
            }
        }
        
        
        //to admin
        $data = [
            'theme'     => 'Новый заказ',
            'email'     => Setting::where('slug', 'siteEmail')->first()->value,
        ];
        $sent = Mail::send('mail.order_toadmin', compact('order'), function($m) use ($data) {
            
            $m->to($data['email'])->subject($data['theme']);
        });
        if($sent === 0) {
            Log::emergency('Ошибка отправки письма с заказом админу: $order => ' . $order->id);
        }
        
    }
}
