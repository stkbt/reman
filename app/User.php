<?php

namespace App;

use App\Core\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements
    AuthorizableContract,
    AuthenticatableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'isActive', 'isSuperAdmin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
        
    public function getIsAdminAttribute() {
        
        return true;
    }
        
        
    public function getNameAttribute() {
        
        return implode(' ', [$this->first_name, $this->last_name]);
    }   
    public function getNicknameAttribute() {
        
        if($this->attributes['nickname'])
            return $this->attributes['nickname'];


        $nickname = '';

        if($this->first_name) $nickname .= substr($this->first_name, 0, 1);
        if($this->last_name) $nickname .= substr($this->last_name, 0, 1);

        return $nickname;
    }

    public function setPasswordAttribute($value) {

        if($value) {

            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function setUserStatusAttribute($value) {

        $this->attributes['isSuperAdmin'] = 0;
        $this->attributes['isAdmin'] = 0;

        switch ($value) {
            case 2:
                $this->attributes['isSuperAdmin'] = 1;
                $this->attributes['isAdmin'] = 1;
                break;
            case 1:
                $this->attributes['isAdmin'] = 1;
                break;
            default:
                
                break;
        }
    }

    public function getUserStatusAttribute() {

        if($this->attributes['isSuperAdmin'] == 1 && $this->attributes['isAdmin'] == 1) {
            return 2;
        }
        elseif($this->attributes['isAdmin'] == 1) {
            return 1;
        }
        else {
            return 0;
        }

    }
}
