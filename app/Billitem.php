<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Utils;

class Billitem extends Model
{
    
    protected $guarded = ['user_id'];

    public function product() {

    	return $this->belongsTo('App\Product');
    }

    public function currency() {
        
        return $this->belongsTo('App\Currency');
    }
    public function bill() {
        
        return $this->belongsTo('App\Bill');
    }

    public function getPriceFormattedAttribute() {

        return number_format($this->attributes['price'], 2, ',', ' ');
    }
    public function setPriceFormattedAttribute($value) {

        $this->attributes['price'] = $value;
    }
    
    public function getPriceRubFormattedAttribute() {

        if($this->currency_id == 1) return number_format($this->attributes['price'], 2, ',', ' ');

        return number_format($this->attributes['price']*$this->currency->rate, 2, ',', ' ');
    }

    public function getPriceRubAttribute() {

        if($this->currency_id == 1) return $this->attributes['price'];

        return $this->attributes['price']*$this->currency->rate;
    }

    public function getTotalRubAttribute() {

        if($this->currency_id == 1) return $this->attributes['price']*$this->attributes['count'];

        return $this->attributes['price']*$this->attributes['count']*$this->currency->rate;
    }
    public function getTotalRubFormattedAttribute() {

        return number_format($this->totalRub, 2, ',', ' ');
    }


    public function getPriceCurAttribute() {

        return $this->priceRub / $this->bill->currency->rate;
    }
    public function getPriceCurFormattedAttribute() {

        return number_format($this->priceCur, 2, ',', ' ');
    }

    public function getTotalCurAttribute() {

        return $this->totalRub / $this->bill->currency->rate;
    }
    public function getTotalCurFormattedAttribute() {

        return number_format($this->totalCur, 2, ',', ' ');
    }

    public function getTotalWeightAttribute() {

        return $this->attributes['weight']*$this->attributes['count'];
    }

    public function getDeliveryTimeAttribute() {

        if(isset($this->attributes['delivery_time_min']))
            $min = $this->attributes['delivery_time_min'];
        
        if(isset($this->attributes['delivery_time_max']))
            $max = $this->attributes['delivery_time_max'];

        if(isset($min) && isset($max))
            return Utils::localizePeriod($min, $max);


        return '–';
    }

}
