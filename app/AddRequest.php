<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddRequest extends Model
{
    //
    protected $guarded = [];

    protected $dates = [
    	'handled_at'
    ];


    protected static function boot() {

        self::creating(function($item){

            $item->sender_id = \Auth::id();
        });
        self::created(function($item){

            $item->sendToVed();
        });

        parent::boot();
    }


    public function search_history() {

    	return $this->belongsTo('App\SearchHistory');
    }

    public function sender() {

    	return $this->belongsTo('App\User', 'sender_id');
    }
    public function handler() {

    	return $this->belongsTo('App\User', 'handler_id');
    }



    public function markAsHandled() {

    	$this->handled_at = \Carbon\Carbon::now();
    	$this->handler_id = \Auth::id();

    	$this->search_history->status = 3;
    	$this->search_history->save();


    	$this->save();

    	$this->sendToManagerComplete();
    }


    public function sendToVed() {

    	$to = \App\Setting::whereSlug('emailVed')->first()->value;

    	\Mail::to($to)->send(new \App\Mail\AddRequestVed($this));
    }
    public function sendToManagerComplete() {

    	$to = $this->sender->email;

    	\Mail::to($to)->send(new \App\Mail\AddRequestComplete($this));
    }
}
