<?php

namespace App\Helpers;

class Utils
{

    private static $periods = [
        'month'  => ['месяцев', 'МЕСЯЦЕВ', 'мес.', 'мес', 'Мес', 'МЕС'],
        'week'  => ['недель', 'НЕДЕЛЬ', 'Недель', 'недели', 'НЕДЕЛИ', 'нед.', 'нед', 'НЕД'],
        'day'   => ['дней', 'ДНЕЙ', 'дня', 'ДНЯ', 'дн.', 'дн', 'Дн'],
        'stock' => ['склад', 'скл.', 'налич']
    ];


    public static function cleanSku($sku) {

        $sku = str_replace('-', '', $sku);
        $sku = str_replace('_', '', $sku);
        $sku = str_replace(' ', '', $sku);

        $sku = strtoupper(str_slug($sku));

        return $sku;
    }
    
    public static function get_http_response_code($url) {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }
    
    public static function strIs($str){
        
        return !(!isset($str) || trim($str) === '');
    }


    public static function localizePeriod($min, $max = null) {

        if($min == 0 && $max == 0) {

            return 'в наличии';
        }

    	$str = '';
        $period_rus = '';

    	$time_unit = self::defineTimeUnit($min);
    	$time_del = 1;

        switch($time_unit) {
            case 'month':

            $time_del = 60*60*24*30;
            $period_rus = 'мес.';

            break;
            case 'week':

            $time_del = 60*60*24*7;
            $period_rus = 'нед.';
            break;
            case 'day':

            $time_del = 60*60*24;
            $period_rus = 'дн.';
            break;
        }

        $min = round($min/$time_del);
        $max = $max ? round($max/$time_del) : null;	

        $str .= $min;

        if($max) {
        	$str .= '-' . $max;
        }

        $str .= ' ' . $period_rus;

        return $str;
    }

    public static function defineTimeUnit($time) {

    	$period = null;
    	$period_rus = null;


    	switch(true) {
    		// case ($time >= 60*60*24*30):
    		// $period = 'month';
    		// break;
    		case ($time >= 60*60*24*7):
    		$period = 'week';
    		break;
    		case ($time >= 60*60*24):
    		$period = 'day';
    		break;
    	}

        return $period;
    }

    public static function parsePeriod($_value) {

        $_value = mb_strtolower($_value);
        
        $val = null;

        $_value = trim($_value);

        if($_value == "" || empty($_value)) {

            return [
                'min' => null,
                'max' => null
            ];
        }

        if($_value == "0" || $_value === 0) {

            return [
                'min' => 0,
                'max' => 0
            ];
        }

        $period = null;

        foreach(self::$periods as $tperiod => $tvars) {

            foreach($tvars as $tvar) {

                if(strstr($_value, $tvar)) {

                    $period = $tperiod;

                    $_value = str_replace($tvar, '', $_value);
                    $_value = trim($_value);
                }
            }
        }


        $time_unit = 0;
        switch($period) {
            case 'month':
            $time_unit = 60*60*24*30;
            break;
            case 'week':
            $time_unit = 60*60*24*7;
            break;
            case 'day':
            $time_unit = 60*60*24;
            break;
            case 'stock':

            return [
                'min' => 0,
                'max' => 0
            ];
            
            break;
        }

        $values = explode('-', $_value);

        $min = $values[0] * $time_unit;

        $max = count($values) == 2 ? $values[1] * $time_unit : $min;

        return [
            'min' => $min,
            'max' => $max
        ];
    }
}
