<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

	protected $fillable = [
        'title'
    ];

    protected static function boot() {

        parent::boot();

        static::addGlobalScope('order', function($query){

        	return $query->orderBy('title', 'asc');
        });

    }

    
    public function products() {
        
        return $this->hasMany('App\Product');
    }
    public function country() {
        
        return $this->belongsTo('App\Country');
    }

    public function getTitleCountryAttribute() {

        $title = $this->title;

        if($this->country) $title .= ' / ' . $this->country->title;

        return $title;
    }
}
