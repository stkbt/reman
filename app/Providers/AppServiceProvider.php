<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use PackageManager;
use Meta;

class AppServiceProvider extends ServiceProvider
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;


    protected $widgets = [
        \App\Widgets\DashboardSearch::class,
        \App\Widgets\HeaderUser::class,
        \App\Widgets\ProductsSearch::class,
        \App\Widgets\BillsFilters::class,
    ];

        
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        $widgetsRegistry = $this->app[\SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface::class];

        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }

        PackageManager::add('reman')
          ->css('reman.css', asset('css/app.css') . '?_nocache=' . time())
          ->js('reman.js', asset('js/app.js') . '?_nocache=' . time())
          ;

        Meta::loadPackage(['reman']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $localProviders = config('app.local_providers', []);
            array_map([$this->app, 'register'], $localProviders);
        }
    }
}
