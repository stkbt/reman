<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Http\AdminSections\Users',
        \App\Product::class => 'App\Http\AdminSections\Products',
        \App\Setting::class => 'App\Http\AdminSections\Settings',
        \App\Brand::class => 'App\Http\AdminSections\Brands',
        \App\Supplier::class => 'App\Http\AdminSections\Suppliers',
        \App\Import::class => 'App\Http\AdminSections\Imports',
        \App\Contractor::class => 'App\Http\AdminSections\Contractors',
        \App\Bill::class => 'App\Http\AdminSections\Bills',
        \App\Billitem::class => 'App\Http\AdminSections\Billitems',
        \App\SearchHistory::class => 'App\Http\AdminSections\SearchHistories',
        \App\Country::class => 'App\Http\AdminSections\Countries',
        \App\Email::class => 'App\Http\AdminSections\Emails',
        \App\ImportProduct::class => 'App\Http\AdminSections\ImportProducts',
        \App\Organization::class => 'App\Http\AdminSections\Organizations',
        \App\AddRequest::class => 'App\Http\AdminSections\AddRequests',
    ];



    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	



        parent::boot($admin);
    }
}
