<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Carbon\Carbon;

use App\Admin\Controllers\ImportController;

use Auth;

use Request;
use MessagesStack;

use App\Import;
use App\ImportProduct;

class Imports extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 600);

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $model->user_id = Auth::id();
        });

        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            
            $import = $model->runImport();

            if($import) {

                MessagesStack::addInfo('<i class="fa fa-check"></i> Импорт файла завершён. Импортировано <big>' . $model->row_last . '</big> позиции.');
            }

            session()->flash('redirect_import_id', $model->id);

        });
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-upload';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Импорт данных';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        if($redirect_import_id = session('redirect_import_id')) {

            return redirect('import_products?import_id=' . $redirect_import_id);
        }


        $table = AdminDisplay::table()
                ->setColumns(
                    AdminColumn::link('title', 'Название'),
                    AdminColumn::datetime('created_at', 'Дата загрузки файла')->setFormat('d.m.Y H:i')->setWidth(200),
                    AdminColumn::text('user.email', 'Пользователь')->setHtmlAttribute('class', 'text-muted')->setWidth(200)
                )
            ;

        $table->setView('display.imports');

        $table->setApply(function ($query) {
            $query
                ->orderBy('created_at', 'desc')
                ;
        });

        $table->paginate(10);

        $table->setNewEntryButtonText('Новый импорт данных');

        // $panel = AdminForm::panel();
        // $panel->setModelClass(Import::class);
        // $panel->setElements([
        //     AdminFormElement::file('filename', 'Файл XLS'),
        //     AdminFormElement::hidden('user_id')->setDefaultValue(\Auth::id()),
        // ])
        // ;

        // $panel->initialize();

        // $panel = $panel->render();

        // echo $panel;

        //view()->share('createForm', $panel);


        return $table;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {

        $instance = null;
        if($id) {

            $instance = Import::find($id);
        }

        $panel = AdminForm::panel();

        $run_btn = null;

        $form = [
            AdminFormElement::text('title', 'Название (комментарий)'),
            AdminFormElement::html('<hr/>'),
            AdminFormElement::html('<h3>Выберите файл для импорта</h3>'),
            AdminFormElement::file('filename', 'Файл XLS (XLSX)')->required(),
            AdminFormElement::hidden('user_id')
        ];

        $import_info = [
                
        ];

        $panel->getButtons()
            ->setSaveButtonText('Загрузить файл и начать импорт')
            ->hideSaveAndCloseButton()
            ->hideSaveAndCreateButton()
        ;

        if($instance) {
            // $run_btn = AdminFormElement::html('<a class="btn btn-warning" href="' . route('admin.import.run') . '?filename=' . $instance->filename . '&import_id=' . $instance->id . '">Запустить импорт</a>');


            $import_info = [
                AdminFormElement::html('<a class="btn btn-default btn-lg" href="/' . $instance->filename . '"><i class="fa fa-cloud-download"></i> Скачать загруженный файл</a>'),
                AdminFormElement::html('<div style="margin-top:42px;"></div>'),
                $instance->hasProducts ? AdminFormElement::html('<a class="btn btn-warning btn-lg" href="/import_products?import_id=' . $instance->id . '"><i class="fa fa-cube"></i> Проверка импортируемых позиций</a>') : AdminFormElement::html('<i class="fa fa-check"></i> Все позиции импортированы или удалены'),
                AdminFormElement::hidden('user_id'),
                AdminFormElement::hidden('filename'),
            ];


            $form = [
                AdminFormElement::text('title', 'Название (комментарий)'),
                AdminFormElement::html('<hr/>'),
                AdminFormElement::html('<h4>Файл загружен: ' . date('d.m.Y H:i:s', strtotime($instance->created_at)) . '</h4>'),
                AdminFormElement::html('<hr/>'),
                AdminFormElement::html('<h4>Позиций в загруженном файле: ' . $instance->rows_total . '</h4>'),
                AdminFormElement::html('<h4>Обработано позиций: ' . $instance->row_last . '</h4>'),
                AdminFormElement::html('<hr/>'),
                AdminFormElement::html('<h4>Статус: ' . ($instance->is_complete ? 'Импорт завершен' : 'В процессе') . '</h4>'),
            ];

            $panel->getButtons()
            ->setSaveButtonText('Сохранить');
        }
        
        
        $panel->addBody([
            AdminFormElement::columns()
            ->addColumn(
                $form
            )
            ->addColumn(
                $import_info
            )
        ]);

        // $panel->addValidationMessage('file.required', 'Выберите файл для загрузки и последующего импорта данных');





        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused


        $import = Import::find($id);

        if(!$import) {

            MessagesStack::addDanger('<i class="fa fa-times"></i> Импорт не найден #' . $id . '');

            return false;
        }

        $fields = [
            'sku', 'sku_alternate', 'title', 'title_en', 'price', 'currency_id', 'delivery_time_min', 'delivery_time_max', 'weight', 'brand_id', 'import_id', 'quantity'
        ];
        

        foreach ($import->import_products->where('is_imported', 1) as $imp_product) {
            
            $prev_imp_product = ImportProduct::where('hash', $imp_product->hash)->where('import_id', '<', $import->id)->orderBy('import_id', 'desc')->first();

            if($prev_imp_product) {

                if($prev_imp_product->duplicate) {

                    foreach ($fields as $field) {
                        
                        $prev_imp_product->duplicate->$field = $prev_imp_product->$field;
                    }

                    $prev_imp_product->duplicate->priced_at = Carbon::now()->toDateTimeString();
                    $prev_imp_product->duplicate->save();
                }
            }
            else {

                if($imp_product->duplicate) {

                    $imp_product->duplicate->delete();
                }
            }

            $imp_product->delete();
        }

        MessagesStack::addInfo('<i class="fa fa-check"></i> Удаление импорта завершено. Товары возвращены в состояние предыдущего импорта');
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        
        return true;
    }


    public function getCreateTitle() {

        return 'Новый импорт из файла XLS / XLSX';
    }

    public function getMessageOnCreate() {

        return '<i class="fa fa-check"></i> Импорт успешно запущен';
    }

    public function can($action, \Illuminate\Database\Eloquent\Model $model) {

        return Auth::user()->isSuperAdmin;
    }
}
