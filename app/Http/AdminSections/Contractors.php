<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Contractor;
use App\Bill;

class Contractors extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 300);


        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $model->user_id = Auth::id();
        });
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-address-book-o';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Контрагенты';
    }




    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        
        $display = AdminDisplay::datatables()
            ->setColumns(
                AdminColumn::link('title', 'Наименование'),
                AdminColumn::text('person', 'Контактное лицо'),
                AdminColumn::text('phone', 'Телефон')
            )
            ;

        $display->setOrder([[0, 'asc']]);

        $display->setNewEntryButtonText('Добавить контрагента');

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $instance = null;
        $bills = null;

        if($id) {
            $instance = Contractor::find($id);
        }

        if($instance) {

            $bills = AdminDisplay::table()
                ->setModelClass(Bill::class)
                ->setApply(function($query) use($id) {
                    $query
                        ->where('contractor_id', $id)
                        ->orderBy('created_at', 'desc')
                        ;
                })
                ->setParameter('contractor_id', $id)
                ->setColumns(
                    AdminColumn::text('id', '#'),
                    AdminColumn::datetime('created_at', 'Создан')->setFormat('d.m.Y H:i'),
                    AdminColumn::text('user.name', 'Менеджер'),
                    AdminColumn::text('itemsCount', 'Позиций')->setHtmlAttribute('class', 'text-muted'),
                    AdminColumn::text('totalRubFormatted', 'Сумма счёта, руб')->setHtmlAttribute('class', 'text-muted'),
                    AdminColumn::text('totalWeight', 'Общий вес, кг')->setHtmlAttribute('class', 'text-muted')->setWidth('140px'),
                    AdminColumn::custom('Заявка', function(Model $model) {
                        
                        if($model->search_history) {

                            return '<a href="/search_histories/?id=' . $model->search_history_id . '">' . $model->search_history->title . '</a>';
                        }
                        
                        return '-';
                    })
                )
                ;


            $this->title = $instance->title;

        }

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use ($instance, $bills) {
            $tabs = [];


            if($bills) {
                $tabs[] = AdminDisplay::tab(
                    AdminForm::elements([
                        $bills
                    ])
                )->setLabel('Ком. предложения');
            }



            $tabs[] = AdminDisplay::tab(
                AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('title', 'Наименование'),
                    AdminFormElement::html('<hr/>'),
                    
                    AdminFormElement::html('<h4>Контактные данные</h4>'),
                    AdminFormElement::text('person', 'Контактное лицо'),
                    AdminFormElement::text('phone', 'Телефон'),
                    AdminFormElement::text('email', 'Email')
                ])
                ->addColumn([
                    AdminFormElement::html('<h4>Адрес</h4>'),
                    AdminFormElement::text('country', 'Страна'),
                    AdminFormElement::text('city', 'Город'),
                    AdminFormElement::text('address', 'Адрес'),
                    AdminFormElement::html('<hr/>'),
                    AdminFormElement::html('<h4>Реквизиты</h4>'),
                    AdminFormElement::text('inn', 'ИНН'),
                    AdminFormElement::text('kpp', 'КПП'),
                    AdminFormElement::text('bank', 'Банковские реквизиты'),
                ])
            )->setLabel('Данные контрагента');


            $tabs[] = AdminDisplay::tab(
                AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('default_paymentmethod', 'Способ оплаты'),
                    AdminFormElement::text('default_delivery_conditions', 'Условия поставки')
                ])
                ->addColumn([
                    AdminFormElement::textarea('default_notes', 'Примечание')->setRows(2),
                ])
            )->setLabel('Значения по умолчанию в КП');


            
            
            
            return $tabs;
        });
        

        $panel = AdminForm::panel();
        

        $panel->addBody([
            $tabs
        ]);

        $panel->getButtons()
            ->hideSaveAndCloseButton()
            ;

        // if(request('_redirectBack')) {
        //     $panel->addBody([
        //         AdminFormElement::hidden('_redirectBack')->setDefaultValue(request('_redirectBack'))
        //     ]);
        // }


        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {

        return Auth::user()->isSuperAdmin == 1;
    }
}
