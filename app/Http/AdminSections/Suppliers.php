<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Supplier;

class Suppliers extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        //$this->addToNavigation($priority = 650);

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-vcard-o';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Поставщики';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        return
            AdminDisplay::table()
                ->setColumns(
                AdminColumn::link('title', 'Наименование')
            )
            ;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $panel = AdminForm::panel();

        
        $panel->addBody([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text('title', 'Наименование'),
            ])
            ->addColumn([
                AdminFormElement::textarea('anounce', '')
            ])
        ]);



        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return false;
    }
}
