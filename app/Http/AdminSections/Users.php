<?php

namespace App\Http\AdminSections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Auth;

class Users extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    
    /**
     * Initialize class.
     */
    public function initialize()
    {
        
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 990, function() {
            return \App\User::count();
        });

        
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-user-circle-o';
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Пользователи';
    }
    
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return
            AdminDisplay::table()
                ->setColumns(
                    AdminColumn::text('id', '#')->setWidth('30px'),
                    AdminColumn::link('name', 'Имя'),
                    AdminColumn::text('email', 'Email')
                )
                ;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $notice = null;

        if($id == Auth::id()) {

            $notice = AdminFormElement::html('<b>Вы редактируете своего пользователя.<br/>Помните, что при изменении роли у вас может пропасть доступ к управлению пользователями!</b>');
        }

        return AdminForm::panel()->addBody([
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('first_name', 'Имя / отчество')->setHelpText('Используется в КП')->required(),
                    AdminFormElement::text('last_name', 'Фамилия')->setHelpText('Используется в КП')->required(),
                    AdminFormElement::text('nickname', 'Символьное обозначение')->setHelpText('Используется при формировании номера поискового запроса'),
                    
                    // AdminFormElement::checkbox('isActive', 'Аккаунт активен (доступ разрешён)'),
                    // AdminFormElement::checkbox('isSuperAdmin', 'Является суперадминистратором (доступно всё)')
                    AdminFormElement::html('<hr/><h4>Роль пользователя</h4>'),
                    AdminFormElement::radio('user_status')->setOptions([
                        0 => 'Менеджер',
                        1 => 'Админ',
                        2 => 'Суперадмин'
                    ])
                    ->setHelpText('– Админ - просматривает всё, не редактирует</br>– Суперадмин - всё редактирует')
                    ->setDefaultValue(0)
                    ->setSortable(false),   

                    $notice
                ])
                ->addColumn([
                    AdminFormElement::text('email', 'Email')->setHelpText('Используется в качестве логина и в качестве адреса отправителя КП')->required(),
                    AdminFormElement::text('phone', 'Телефон')->setHelpText('Используется в КП')->required(),
                    AdminFormElement::password('password', 'Новый пароль'),
                ])
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function can($action, \Illuminate\Database\Eloquent\Model $model) {

        return Auth::user()->isSuperAdmin || Auth::user()->isAdmin;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model) {

        return Auth::user()->isSuperAdmin == 1;
    }
}
