<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminDisplayFilter;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Display\ControlButton;
use SleepingOwl\Admin\Display\ControlLink;

use Request;

use App\Import;
use App\Product;
use App\ImportProduct;
use App\Bill;
use App\Supplier;
use App\Brand;
use App\Currency;
use App\Scopes\VisibleScope;

use DB;

class ImportProducts extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    
    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        // $this->addToNavigation($priority = 610);
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-cube';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Импортируемые товары / ' . $this->title;
    }

    public function isCreatable() {

        return false;
    }
    public function isEditable(Model $model) {

        return true;
    }
    public function isDeletable(Model $model) {

        return request('is_search') == null;
    }
    
    
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        
        $display = AdminDisplay::datatablesAsync();


        if($import_id = request('import_id')) {

            $import = Import::find($import_id);

            $this->title = 'Импорт #' . $import->id . ' от ' . $import->created_at->format('d.m.Y H:i') . ' / ' . $import->title;

            $display->setApply(function ($query) use ($import_id) {

                $query
                    ->where('import_products.import_id', $import_id)
                ;
            });


            $is_duplicate = request('is_duplicate', 'all');

            if($is_duplicate === '1') {
                $is_duplicate = 1;
            }
            else if($is_duplicate === '0') {
                $is_duplicate = 0;
            }

            if($is_duplicate === 1 || $is_duplicate === 0) {

                $display->getApply()->push(function ($query) use ($is_duplicate) {
                    $query->where('is_duplicate', $is_duplicate);
                });
            }


            $display->setActions([
                AdminColumn::action('updateImportProducts', 'Импортировать выбранные')
                    ->setIcon('fa fa-check')
                    ->setAction(route('admin.updateImportProducts'))
                    ->setHtmlAttribute('class', 'btn-success')
                    ->setHtmlAttribute('id', 'updateImportProducts'),
                AdminColumn::action('removeImportProducts', 'Удалить выбранные из импорта')
                    ->setIcon('fa fa-remove')
                    ->setAction(route('admin.removeImportProducts'))
                    ->setHtmlAttribute('class', 'btn-danger')
                    ->setHtmlAttribute('id', 'removeImportProducts')
                    ,

            ]);

            $display->getActions()->setHtmlAttribute('class', 'import_products_actions  panel-footer');
        }


        $display->setColumns(
            // AdminColumn::image('image', '')->setWidth('108px'),
            AdminColumn::checkbox(),
            AdminColumn::text('sku', 'Артикул'),
            AdminColumn::text('sku_alternate', 'Альт. номер'),
            AdminColumnEditable::text('title', 'Название')->append(AdminColumn::text_duplicate('duplicate.title')),
            AdminColumnEditable::text('title_en', 'Название англ')->append(AdminColumn::text_duplicate('duplicate.title_en')),
            AdminColumnEditable::text('price_formatted', 'Цена')->setWidth('108px')->append(AdminColumn::text_duplicate('duplicate.price_formatted')),
            AdminColumn::text('currency.title', 'Валюта')->setHtmlAttribute('class', 'text-muted')->append(AdminColumn::text_duplicate('duplicate.currency.title')),
            AdminColumn::text('price_rub', 'Цена, руб.')->setWidth('108px')->append(AdminColumn::text_duplicate('duplicate.price_rub')),
            AdminColumn::text('quantity', 'Кол-во')->setWidth('80px')->append(AdminColumn::text_duplicate('duplicate.quantity')),
            AdminColumn::text('delivery_time', 'Срок поставки')->append(AdminColumn::text_duplicate('duplicate.delivery_time')),
            AdminColumnEditable::text('weight', 'Вес')->append(AdminColumn::text_duplicate('duplicate.weight')),
            // AdminColumn::text('supplier.title', 'Поставщик')->setHtmlAttribute('class', 'text-muted'),
            AdminColumn::text('brand.title_country', 'Производитель')->setHtmlAttribute('class', 'text-muted')->append(AdminColumn::text_duplicate('duplicate.brand.title_country'))
            // AdminColumn::text('is_duplicate', '')
            // AdminColumn::datetime('priced_at', 'Цена обновлена')->setHtmlAttribute('class', 'text-muted'),
            // AdminColumn::text('price', '')->setWidth('0px'),
            // AdminColumn::text('delivery_time_min', '')->setWidth('0px')
        )
        ;

        // $display->setColumnFilters([
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     AdminColumnFilter::select()->setPlaceholder('brand')->setModel(new Brand)->setDisplay('title'),
        //     AdminColumnFilter::select()->setPlaceholder('has dupl')->setOptions([0 => "no", 1 => "yes"])
        // ]);


        $display->with('duplicate');

        $display->setHtmlAttribute('id', 'products');
        $display->setHtmlAttribute('class', 'import_products');

        $display->addStyle('products.css', resources_url('css/products.css'), ['admin-default']);
        $display->addScript('products.js', resources_url('js/products.js'), ['admin-default']);


        $import_products_extension = new \App\Admin\Extensions\ImportProductsExtension();
        $import_products_extension->push(
            AdminColumn::action('updateImportProducts', 'Импортировать все')
                    ->setIcon('fa fa-check')
                    ->setAction(route('admin.updateImportProducts') . '?import_id='.$import_id.'&all=1')
                    ->setHtmlAttribute('class', 'btn-success')
                    ->setHtmlAttribute('id', 'updateImportProducts')
        );
        $import_products_extension->setHtmlAttribute('class', 'import_products_extension');

        $display->extend('import_products_extension', $import_products_extension);
        
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $instance = null;
        if($id) {
            
            $instance = ImportProduct::find($id);

            $this->title =  $instance->title;
        }
        
        $panel = AdminForm::panel();

        $panel->getButtons()
            ->hideSaveAndCloseButton()
            ->hideSaveAndCreateButton()
            ;
        
        $panel->addBody(
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('title', 'Название')->required(),
                ])
                ->addColumn([
                    AdminFormElement::text('title_en', 'Название англ.')->required(),
                ]),
            AdminFormElement::html('<hr/>'),    
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('sku', 'Артикул'),
                    AdminFormElement::text('sku_alternate', 'Альтернативный номер'),
                    AdminFormElement::select('brand_id', 'Производитель')->setModelForOptions('App\Brand'),
                    AdminFormElement::html('<hr/>'),
                    AdminFormElement::text('weight', 'Вес'),
                ])
                ->addColumn([
                    AdminFormElement::text('price', 'Цена')->setDefaultValue(0),
                    AdminFormElement::html('<div class="form-inline form-inline-radio" id="billcurrency">'),    
                    AdminFormElement::radio('currency_id', 'Валюта')->setModelForOptions('App\Currency'),
                    AdminFormElement::html('</div>'),
                    AdminFormElement::html('<hr/>'),
                    AdminFormElement::text('delivery_time', 'Срок поставки'),
                ])
                ->addColumn([
                    // AdminFormElement::html('<h4>Изображение</h4>'),
                    // AdminFormElement::image('image'),
                ])
        );
        
        
        
        
        // $path = url('/') . '/.../';
        
        // if($instance && $instance->category) $path = url($instance->category->url) . '/';
        
        // $panel->addFooter(
        //     AdminForm::elements([
        //         AdminFormElement::textaddon('slug', 'Адрес на сайте / формируется автоматически от названия')
        //         ->setAddon($path)
        //         ->placeBefore()
        //         ,
        //     ])
        // );
        
        
        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {

        return redirect()->route('admin.updateImportProducts');
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }


    
}
