<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Bill;
use App\Billitem;

class Billitems extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        // $this->addToNavigation($priority = 750);


    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Позиции счёта';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        return
            AdminDisplay::table()
                ->setColumns(
                    AdminColumn::text('id', '')->setWidth('0px')->setHtmlAttribute('class', '_id width-0'),
                    AdminColumn::text('product.sku', 'Артикул'),
                    AdminColumnEditable::text('title', 'Название'),
                    AdminColumn::link('product.title_en', 'Название англ'),
                    AdminColumnEditable::text('price_formatted', 'Цена продажи'),
                    AdminColumn::text('currency.title', 'Валюта продажи')->setHtmlAttribute('class', 'text-muted'),
                    AdminColumnEditable::text('count', 'Кол-во'),
                    AdminColumn::text('total_rub', 'Стоимость, руб.')->setWidth('108px'),
                    AdminColumn::text('product.delivery_time', 'Срок поставки'),
                    AdminColumn::text('product.brand.title_country', 'Производитель')->setHtmlAttribute('class', 'text-muted'),
                    AdminColumn::relatedLink('bill.id', 'Счёт')
                )
                ;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $panel = AdminForm::panel();

        
        $panel->addHeader([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::select('bill_id', 'Счёт')->setModelForOptions('App\Bill')
            ])
            ->addColumn([
                
            ])
        ]);



        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return true;
    }
    public function isCreatable() {
        return false;
    }
    public function isEditable(Model $model) {
        return true;
    }
}
