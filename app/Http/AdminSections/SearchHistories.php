<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminDisplayFilter;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Display\ControlButton;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\SearchHistory;
use App\User;
use App\Contractor;

class SearchHistories extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 150);

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-history';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'История поиска';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        // $display =
        //     AdminDisplay::datateblesAsyncExteneded()
        //     ;

        
        // $display = AdminDisplay::datatebles_withnotfound('display.extensions.columns_withstatus');
        // $display = AdminDisplay::datatablesAsyncExteneded('display.extensions.columns_withstatus');
        $display = AdminDisplay::datatablesAsync();

        //TODO: make with status from column view (for async)



        // $display->setDatatableAttributes([
        //     "aoColumns" => [
        //         [ "sTitle" => '', "bSortable" => false ],
        //         [ "sTitle" => '', "bSortable" => false ],
        //         [ "sTitle" => '', "bSortable" => false ],
        //         [ "sTitle" => '', "bSortable" => false ],
        //         [ "sTitle" => '', "bSortable" => false ],
        //         [ "sTitle" => '', "bSortable" => false ],
        //     ]
        // ]);


        $display->setHtmlAttribute('id', 'search_histories');

        // $display->setOrder([[0, 'desc']]);

        $columns = [
            
            AdminColumn::text('id', '#')->setOrderable(false)->setWidth('4px'),

            AdminColumnEditable::text('title', '№ Заявки')->setOrderable(false)
                // ->setSearchCallback(function($column, $query, $search){

                //     $query->where('search_histories.title', 'like', "%$search%");
                // })
            ,
            
            AdminColumn::datetime('created_at', 'Дата')
                ->setFormat('d.m.Y H:i')
                ->setHtmlAttribute('class', 'text-muted')->setOrderable(false)
                ->setWidth('180px')
                ,

            AdminColumn::text('total', 'Сумма, руб')->setOrderable(false),
            AdminColumn::text('weight', 'Общий вес, кг')->setOrderable(false),

            AdminColumnEditable::text('comment', 'Комментарий')->setOrderable(false)
        ];

        // if(Auth::user()->isSuperAdmin) 
            $columns[] = AdminColumn::relatedLink('user.name', 'Менеджер')->setWidth('160px');
        
        // $columns[] = AdminColumn::relatedLink('contractor.title', 'Контрагент')->setWidth('140px');

        $columns[] = AdminColumn::custom('Клиент', function(Model $model){

                if(!$model->contractor) return null;

                return '<a style="white-space:nowrap;" class="label label-default" href="/contractors/'.$model->contractor->id.'/edit">' . $model->contractor->title . '</a>';
            })
            ;

        $columns[] = AdminColumn::custom('КП', function(Model $model){

                $res = '<div style="line-height:1.6">';

                foreach($model->bills as $bill) {
                    $res .= '<a class="label label-default" href="/bills/'.$bill->id.'/edit">' . $bill->title_short . '</a><br/>';
                }

                $res .= '</div>';

                return $res;
            })
            ->setWidth('180px')
            ;


        $columns[] = AdminColumn::text('status')->setOrderable(false)->setWidth('1px');
        // $columns[] = AdminColumn::text('status')->setOrderable(false)->setWidth('1px');



        $display->setColumns($columns);


        $control = $display->getColumns()->getControlColumn()->setWidth(400);


        $button_retry = new ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           
           return route('admin.searchHistorySubmit', $model->getKey());
        
        }, 'Повторить', 50);
        $button_retry->setMethod('post');
        $button_retry->setIcon('fa fa-search');
        $button_retry->setHtmlAttribute('class', 'btn-default');


        $button_view = new ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           
           return route('admin.searchHistoryView', $model->getKey());
        
        }, 'Просмотр', 40);
        
        $button_view->setMethod('get');
        $button_view->setIcon('fa fa-eye');
        $button_view->setHtmlAttribute('class', 'btn-default btn-view');
        
        $button_download = new ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           
           return route('admin.searchHistoryDownload', $model->getKey());
        
        }, '', 40);
        
        $button_download->setMethod('get');
        $button_download->setIcon('fa fa-download');
        $button_download->setHtmlAttribute('class', 'btn-default');
        $button_download->setHtmlAttribute('data-original-title', 'Скачать');


        $button_sendved = new ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           
           return route('admin.searchHistorySendVed', $model->getKey());
        
        }, 'ВЭД', 40);
        $button_sendved->setMethod('get');
        $button_sendved->setIcon('fa fa-share-square-o');
        $button_sendved->setHtmlAttribute('class', 'btn-warning btn-sendved');
        $button_sendved->setHtmlAttribute('title', 'Отправить в отдел ВЭД');



        $control->addButton($button_sendved);
        $control->addButton($button_view);
        $control->addButton($button_retry);
        $control->addButton($button_download);



        $filters = [];


        $filters[] = AdminDisplayFilter::field('id')->setOperator('equal')
                    ->setTitle(function($value) {
                        return "Заявка: ". $value;
                    });

        if(request('user_id') && !empty(request('user_id'))) {

            $user = User::find(request('user_id'));

            $filters[] = AdminDisplayFilter::field('user_id')
                ->setOperator('equal')
                ->setValue(request('user_id'))
                ->setTitle(function($value) use ($user) {
                    return "Менеджер: ". ($user ? $user->name : '#' . request('user_id'));
                });
        }

        if(request('contractor_id') && !empty(request('contractor_id'))) {

            $contractor = Contractor::find(request('contractor_id'));

            $filters[] = AdminDisplayFilter::field('contractor_id')
                ->setOperator('equal')
                ->setValue(request('contractor_id'))
                ->setTitle(function($value) use ($contractor) {
                    return "Контрагент: ". ($contractor ? $contractor->title : '#' . request('contractor_id'));
                });
        }

        $display->setFilters($filters);

        $display->setApply(function ($query) {
            $query
                ->orderBy('created_at', 'desc')
                ;
        });


        $display->addScript('products.js', resources_url('js/search_histories.js'), ['admin-default']);



        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
         
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        
        return Auth::user()->user_status > 1;
    }
    public function isEditable(Model $model) {
        return true;
    }
    public function isCreatable() {
        return false;
    }



}
