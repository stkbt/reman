<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Organization;
use App\Bill;

class Organizations extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 800);


        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // $model->user_id = Auth::id();
        });
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-address-book-o';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Организации';
    }




    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        
        $display = AdminDisplay::datatables()
            ->setColumns(
                AdminColumn::link('name', 'Наименование'),
                AdminColumn::text('inn', 'ИНН')
            )
            ;

        $display->setOrder([[0, 'asc']]);

        $display->setNewEntryButtonText('Добавить организацию');

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $instance = null;
        $bills = null;

        if($id) {
            $instance = Organization::find($id);
        }

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use ($instance, $bills) {
            
            $tabs = [];

            $tabs[] = AdminDisplay::tab(
                AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('name', 'Наименование'),
                    AdminFormElement::html('<hr/>'),
                    
                    AdminFormElement::text('inn', 'ИНН'),
                    AdminFormElement::text('kpp', 'КПП'),
                    AdminFormElement::text('ogrn', 'ОГРН'),
                    AdminFormElement::text('address', 'Адрес'),
                ])
                ->addColumn([
                    AdminFormElement::textarea('director', 'Руководитель (строка подписи в КП)')->setRows(3),
                    AdminFormElement::html('<hr/>'),

                    AdminFormElement::file('form_file', 'Бланк КП')
                        ->setHelpText('Бланк КП должен соответствовать шаблону &rarr; <a href="/assets/kp.xlsx">Бланк КП</a>')
                ])
            )->setLabel('Данные организации');


            // $tabs[] = AdminDisplay::tab(
            //     AdminFormElement::columns()
            //     ->addColumn([
            //         AdminFormElement::text('default_paymentmethod', 'Способ оплаты'),
            //         AdminFormElement::text('default_delivery_conditions', 'Условия поставки')
            //     ])
            //     ->addColumn([
            //         AdminFormElement::textarea('default_notes', 'Примечание')->setRows(2),
            //     ])
            // )->setLabel('Значения по умолчанию в КП');


            
            // if($bills) {
            //     $tabs[] = AdminDisplay::tab(
            //         AdminForm::elements([
            //             $bills
            //         ])
            //     )->setLabel('Счета');
            // }
            
            return $tabs;
        });
        

        $panel = AdminForm::panel();
        

        $panel->addBody([
            $tabs
        ]);

        $panel->getButtons()
            ->hideSaveAndCloseButton()
            ;

        // if(request('_redirectBack')) {
        //     $panel->addBody([
        //         AdminFormElement::hidden('_redirectBack')->setDefaultValue(request('_redirectBack'))
        //     ]);
        // }


        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {

        return Auth::user()->isSuperAdmin == 1;
    }
}
