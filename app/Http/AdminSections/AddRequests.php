<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Display\ControlButton;
use SleepingOwl\Admin\Display\ControlLink;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\AddRequest;

class AddRequests extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = null;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 900);


        //mark_as_handled


        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            
            if(request('next_action') == 'mark_as_handled') {

                $model->markAsHandled();
                $model->save();

                return redirect('/add_requests');
            }
        });
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-plus';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Запросы на добавление';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = 
            AdminDisplay::datatables()
                ->setColumns(
                    AdminColumn::link('search_history.title', 'Номер заявки'),
                    AdminColumn::link('sender.name', 'Менеджер'),
                    AdminColumn::datetime('created_at', 'Запрос отправлен'),
                    AdminColumn::custom('Запрос обработан', function(\Illuminate\Database\Eloquent\Model $model) {

                        return !is_null($model->handled_at) ? '✔' : '';
                    })->setWidth(140)
                )

            ;

        $display->setApply(function ($query) {
            $query
                ->orderBy('created_at', 'desc')
                ;
        });



        $control = $display->getColumns()->getControlColumn()->setWidth(400);
        
        // $control = $display->getColumns()->getControlColumn()->setWidth(200);


        $button_download = new ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
           return route('admin.searchHistoryDownload', $model->search_history_id);
        }, 'Скачать заявку', 40);
        $button_download->setMethod('get');
        $button_download->setIcon('fa fa-download');
        $button_download->setHtmlAttribute('class', 'btn-default');
        $button_download->setHtmlAttribute('data-original-title', 'Скачать заявку');

        $control->addButton($button_download);


        $button_edit = new ControlLink(function (\Illuminate\Database\Eloquent\Model $model) {
           return '/add_requests/' . $model->getKey() . '/edit';
        }, 'Просмотр запроса', 40);
        $button_edit->setIcon('fa fa-eye');
        $button_edit->setHtmlAttribute('class', 'btn-primary');

        $control->addButton($button_edit);


        $control->setEditable(false);
        // $display->getColumns()->setControlColumn($control);



        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $instance = AddRequest::find($id);

        $panel = AdminForm::panel();

        
        $panel->addFooter([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::html('<h4><small>Заявка:</small></h4>'),
                AdminFormElement::html('<h4><small>Менеджер:</small></h4>'),
                AdminFormElement::html('<h4><small>Комментарий:</small></h4>'),
            ], 1)
            ->addColumn([
                AdminFormElement::html('<h4 style="padding-left:12px;"><a href="/search_histories?id='.$instance->search_history_id.'">'.$instance->search_history->title.'</a></h4>'),
                AdminFormElement::html('<h4 style="padding-left:12px;">'.$instance->sender->name.'<small> / <a href="mailto:'.$instance->sender->email.'">'.$instance->sender->email.'</a> / '.$instance->sender->phone.'</small></h4>'),
                AdminFormElement::html('<h4 style="padding-left:12px;"><small style="color:#111111">'.nl2br($instance->sender_message).'</small></h4>'),

            ], 5)
            ->addColumn([
                AdminFormElement::textarea('handler_message', 'Оставить комментарий')->setRows(2),
                AdminFormElement::html('
                    <button type="submit" name="next_action" value="mark_as_handled" class="btn btn-success">
                        <i class="fa fa-check"></i> Отметить запрос как выполненный
                    </button>
                ')
            ]),
            AdminFormElement::html('<div style="height:48px;">&nbsp;</div>')
        ]);



        $search_history_view = \App\Admin\Controllers\SearchController::getSearchHistoryView($instance->search_history);


        $panel->addBody([
            AdminFormElement::html('<div style="overflow:hidden"><a class="btn btn-default" href="'.route('admin.searchHistoryDownload', $instance->search_history_id).'" title="" data-toggle="tooltip" data-original-title="Скачать заявку">
                    <i class="fa fa-download"></i>
                    Скачать в Excel
            </a><h3 class="pull-left" style="margin-top:4px;margin-right:24px;">Исходная заявка</h3></div>'),
            AdminFormElement::html($search_history_view->render())
        ]);


        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return false;
    }

    public function isCreatable() {
        return false;
    }
}
