<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use SleepingOwl\Admin\Form\FormButtonsEmail;

use Auth;

use Request;

use App\Email;
use App\Bill;

class Emails extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        // $this->addToNavigation($priority = 600);

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-envelope-o';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Emails';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display =
            AdminDisplay::table()
                ->setColumns(
                    AdminColumn::datetime('created_at', 'Создано')->setFormat('d.m.Y H:i')->setWidth('140px'),
                    AdminColumn::lists('to', 'Получатели'),
                    AdminColumn::text('bill.contractor.title', 'Контрагент'),
                    AdminColumn::relatedLink('bill.id', 'Счёт №<small style="line-height:1.1;font-weight:normal;display:block;">По клику - переход к счёту</small>'),
                    AdminColumn::relatedLink('user.name', 'Пользователь')
                )
            ;

        $display->setNewEntryButtonText('Создать e-mail');

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $panel = AdminForm::panel();

        $theme_default = 'Коммерческое предложение';
        $to = [];
        
        $instance = null;
        if($id) {

            $instance = Email::find($id);
        }

        $bill_id = $instance ? $instance->bill_id : request('bill_id');

        if($bill_id) {

            $bill = Bill::find($bill_id);

            if($bill) {

                $theme_default .= ' №' . $bill->numberFormatted . ' от ' . date('d.m.Y', strtotime($bill->created_at));

                $to[] = $bill->contractor->email;

                $to = collect($to)->merge(Email::autoTo($bill))->filter()->all();
            }
        }

        $to = collect($to)->combine($to)->all();


        $panel->addBody([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::multiselect('to', 'Получатели', $to)->taggable()->setDefaultValue([array_get(array_values($to), 0)])->setHelpText('Можно добавить несколько получателей, в том числе не присутствующих в выпадающем списке.<br/>В список помещены все адреса, на которые когда-либо были отправлены КП текущему контрагенту.')->required(),
                AdminFormElement::text('theme', 'Тема письма')->setDefaultValue($theme_default),
                AdminFormElement::wysiwyg('body', 'Текст письма')
                    ->setHeight(50)
                    ->setEditor('ckeditor')
                    ->setParameters([
                        'toolbarGroups' => [
                            [ 'name' => 'basicstyles' ],
                            // [ 'name' => 'links' ],
                            [ 'name' => 'paragraph', 'groups' => [ 'list', 'align' ] ]
                        ],
                        'removePlugins' => 'elementspath'
                    ])
                    ,
            ])
            ->addColumn([
                AdminFormElement::select('bill_id', 'Прикрепить коммерческое предложение')->setModelForOptions('App\Bill'),
                AdminFormElement::hidden('attachment_type'),
                AdminFormElement::html(
                    view('admin.radioformats', [
                        'options' =>   [
                                            // 'pdf' => '<i class="fa fa-file-pdf-o fa-2x"></i> PDF',
                                            'xls' => '<i class="fa fa-file-excel-o fa-2x"></i> Excel',
                                        ],
                        'name' => 'attachment_type',
                        'value' => 'xls'
                        ])
                )
            ])
        ]);


        $formButtons = new FormButtonsEmail;

        if(!$instance || !$instance->sended_at) {
            $formButtons->setSaveButtonText('Отправить письмо');
        }
        else {
            $formButtons->setSaveButtonText('Отправить письмо повторно');
        }
        

        if($bill_id) {
            $formButtons->setBackUrl('/bills/' . $bill_id . '/edit');
            $formButtons->setCancelButtonText('Отменить и вернуться к КП');
        }

        $panel->setButtons($formButtons);


        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return false;
    }

    public function getCreateTitle()
    {
        $title = 'Отправить по электронной почте';

        if($bill_id = request('bill_id')) {

            // $title .= ' / КП №' . $bill_id;
        }

        return $title;
    }


    public function getMessageOnCreate() {

        return 'Письмо успешно отправлено';
    }
}
