<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Brand;
use App\Country;

class Brands extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500);

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-trademark';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Производители';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display =
            AdminDisplay::table()
                ->setColumns(
                    AdminColumn::link('title', 'Название'),
                    AdminColumn::text('country.title', 'Страна'),
                    AdminColumn::text('notes', 'Комментарий')
                )
            ;

        $display->setNewEntryButtonText('Добавить производителя');



        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
        $panel = AdminForm::panel();

        
        $panel->addBody([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text('title', 'Название'),
                AdminFormElement::textarea('notes', 'Комментарий')->setRows(2)
            ])
            ->addColumn([
                AdminFormElement::select('country_id', 'Страна')->setModelForOptions('App\Country')
            ])
            ->addColumn([
                AdminFormElement::html('<hr style="margin-top:6px;opacity:0;"/>'),
                AdminFormElement::html('<a href="/countries/create">Добавить новую страну</a>')
            ])
        ]);



        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return Auth::user()->isSuperAdmin;
    }
    public function isEditable(\Illuminate\Database\Eloquent\Model $model) {

        return Auth::user()->isSuperAdmin;
    }
}
