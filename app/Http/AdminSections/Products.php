<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminDisplayFilter;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Display\ControlButton;
use SleepingOwl\Admin\Display\ControlLink;

use Request;
use Auth;

use App\Product;
use App\Bill;
use App\Supplier;
use App\Brand;
use App\Currency;
use App\Scopes\VisibleScope;

use DB;

class Products extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    
    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 400);
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-cube';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Товары';
    }

    public function isCreatable() {

        return false;
    }
    public function isEditable(Model $model) {

        return Auth::user()->isSuperAdmin == 1;
    }
    public function isDeletable(Model $model) {

        // return request('is_search') == null && Auth::user()->isSuperAdmin == 1;
        return Auth::user()->isSuperAdmin == 1;
    }
    
    
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $sku = request('sku', old('sku'));
        $brand_id = request('brand_id', old('brand_id'));
        $counts = request('count', old('count', []));

        $title = request('title', old('title'));
        $search_history_id = request('search_history_id', old('search_history_id'));
        $contractor_id = request('contractor_id', old('contractor_id'));

        $search_alternate = null;

        if($sku || $brand_id || $title) {

            $display = AdminDisplay::datatebles_withnotfound();

            $display->disablePagination();

            $filters = [];

            if($sku) {

                $search_alternate = request('search_alternate', old('search_alternate'));
                
                $filters[] = AdminDisplayFilter::custom('sku')
                    ->setValue($sku)
                    ->setCallback(function($query, $value) use ($search_alternate) {
                        
                        if($search_alternate) {

                            $query->where(function($query) use ($value){

                                    return $query
                                        ->orWhereIn('products.sku', $value)
                                        ->orWhereIn('products.sku_alternate', $value)
                                        ;
                                })
                                ;
                        }
                        else {

                            $query
                                ->whereIn('products.sku', $value);
                        }

                    })
                    ->setTitle(function($value) {
                        return "Искомые артикулы: ". implode(', ', $value);
                    })
                ;

                if($search_alternate) {

                    $filters[] = AdminDisplayFilter::custom('search_alternate')
                        ->setValue($search_alternate)
                        ->setCallback(function($query, $value) use ($search_alternate) {
                            
                        })
                        ->setTitle(function($value) {
                            return "✓ С учётом альтернативных номеров";
                        })
                    ;
                }



                $display->setSeachers('sku', $sku);

                if($search_alternate) {

                    $display->setSeachersFieldAdditional('sku', 'sku_alternate');
                }

            }


            if($brand_id) {

                $brands = Brand::whereIn('id', $brand_id)->pluck('title', 'id');

                $filters[] = AdminDisplayFilter::field('brand_id')->setOperator('in')->setValue($brand_id)
                    ->setTitle(function($value) use ($brands) {
                        return "Фильтр по брендам: ". implode(', ', $brands->values()->all());
                    });
            }

            if($title) {


                $filters[] = AdminDisplayFilter::field('title')->setOperator('contains')->setValue($title)->setTitle(function($value) {
                    return "Поиск по наименованию: ". $value;
                });
            }




            $display->setFilters($filters);



        }
        else {

            $display = AdminDisplay::datatablesAsync();
        }


        $display->setApply([

            function($query) {

                $query->latest('priced_at');
            },
            function($query) use ($brand_id) {

                // $query->groupBy('sku');

                // if($brand_id) {

                //     // dd($brand_id);

                //     $query->whereIn('brand_id', []);
                // }
            }

        ]);



        $display->setDatatableAttributes([
            // "aoColumns" => [
            //     [ "bSortable" => false ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => true, "iDataSort" => 12],
            //     // [ "bSortable" => false ],
            //     [ "bSortable" => true, "iDataSort" => 12 ],
            //     [ "bSortable" => true,  "iDataSort" => 13  ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => false ],
            //     [ "bSortable" => true ],
            //     [ "bSortable" => false ],
            //     // [ "bSortable" => true, "bVisible" => false ],
            //     [ "bSortable" => false, "bVisible" => false ],
            //     [ "bSortable" => false, "bVisible" => false ],
            //     [ "bSortable" => true, "bVisible" => false ],
            //     [ "bSortable" => false, "bVisible" => true ],
            //     [ "bSortable" => false, "bVisible" => true ],
            // ],
            "autoWidth" => true
        ]);
        
              

        $display->setColumns([
            // AdminColumn::image('image', '')->setWidth('108px'),
            AdminColumn::checkbox(),
            AdminColumn::text('sku', 'Артикул')->setWidth('120px')->setOrderable(false),
            AdminColumn::text('sku_alternate', 'Альт. номер')->setWidth('120px')->setOrderable(false),
            AdminColumn::text('title', 'Название'),
            AdminColumn::text('title_en', 'Название англ'),
            AdminColumn::text('price_formatted_currency', 'Цена')->setWidth('108px')->setOrderable('price'),
            // AdminColumn::text('currency.title', 'Валюта')->setHtmlAttribute('class', 'text-muted'),
            AdminColumn::text('price_rub', 'Цена, руб.')->setWidth('108px')->setOrderable('price'),
            AdminColumn::text('delivery_time', 'Срок поставки')->setOrderable('delivery_time_min'),
            AdminColumn::text('quantity', 'На складе'),
            AdminColumn::text('weight', 'Вес'),
            AdminColumn::text('brand.title_country', 'Производитель')
                ->setHtmlAttribute('class', 'text-muted')
                ->setOrderable(false),
            AdminColumn::custom('Запр. кол-во', function(Model $model) use ($sku, $counts, $search_alternate) {
                    
                    $i = collect($sku)->search(strtoupper($model->sku));

                    if($i === false && $search_alternate) {

                        $i = collect($sku)->search(strtoupper($model->sku_alternate));
                    }

                    if($i !== false) return array_get($counts, (int)$i, '--');
                    
                    return '-';
                })
                ->setHtmlAttribute('class', 'text-muted _count')
        ])
        ;


        if(Auth::user()->user_status > 0) {

            $display->getColumns()->push(
                AdminColumn::datetime('priced_at', 'Цена обновлена')->setHtmlAttribute('class', 'text-muted')
            );
        }

        
            


        $bill_id = old('bill_id', request('bill_id'));
        
        $display->setActions([

            AdminColumn::action('createBill', 'Создать КП из выбранных')
                ->setIcon('fa fa-share')
                ->setAction(route('admin.createbill', ['search_history_id' => $search_history_id, 'contractor_id' => $contractor_id]))
                ->setHtmlAttribute('id', 'createBill'),

            AdminColumn::actionselect('addToBill', 'Добавить к КП')
                ->setIcon('fa fa-share')
                ->setAction(route('admin.addToBill'))
                ->setHtmlAttribute('id', 'addToBill')
                ->setModelForOptions('App\Bill')
                ->setSelectName('bill_id')
                ->setSelectValue($bill_id)
                ->setLoadOptionsQueryPreparer(function($element, $query) {
                    return $query
                        ->orderBy('created_at', 'desc')
                        ;
                })
                ->setDisplay('title')
                ->setPreTitle(' или добавить к существующему КП')
                ,
        ]);

        $display->getActions()->setHtmlAttribute('class', 'products_actions panel-footer');


        if($bill_id) {
            
            $control = $display->getColumns()->getControlColumn();

            $button = new ControlLink(function (Model $model) {
            
                return '/addToBill?_id[]=' . $model->getKey();

            }, 'Добавить к счёту №' . $bill_id, 50);

            $button
                ->hideText()
                ->setIcon('fa fa-share')
                ->setHtmlAttribute('class', 'btn-warning add_to_bill')
                ->setHtmlAttribute('data-bill_id', $bill_id)
                ->setHtmlAttribute('data-method', 'post')
                ;

            $control->addButton($button);

        }
        
        
        



        // $display->setColumnFilters([
        //     null,
        //     AdminColumnFilter::text()->setPlaceholder('Артикул')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:108px']),
        //     AdminColumnFilter::text()->setPlaceholder('Название')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:144px']),
        //     AdminColumnFilter::text()->setPlaceholder('Название англ.')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:144px']),
        //     null,
        //     null,
        //     AdminColumnFilter::range()->setFrom(
        //         AdminColumnFilter::text()->setPlaceholder('Цена в рублях от')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:80px;'])
        //     )->setTo(
        //         AdminColumnFilter::text()->setPlaceholder('Цена в рублях до')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:80px;'])
        //     ),
        //     AdminColumnFilter::select()->setPlaceholder('Поставщик')->setModel(new Supplier)->setDisplay('title')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:100%']),
        //     AdminColumnFilter::select()->setPlaceholder('Производитель')->setModel(new Brand)->setDisplay('title')->setHtmlAttributes(['class' => 'input-sm', 'style' => 'width:100%']),
        // ]);



        // $display->setApply(function ($query) {
        //     $query
        //         ->orderBy('priced_at', 'desc')
        //         ;
        // });
        

            // dd($display->getFilters());



        $display->setHtmlAttribute('id', 'products');

        $display->addStyle('products.css', resources_url('css/products.css?_nocache=4'), ['admin-default']);
        $display->addScript('products.js', resources_url('js/products.js?_nocache=4'), ['admin-default']);
        


        $display->addStyle('ionrangeslider.css', '/packages/ionrangeslider/css/ion.rangeSlider.css', ['admin-default']);
        $display->addStyle('ionrangesliderskin.css', '/packages/ionrangeslider/css/ion.rangeSlider.skinFlat.css', ['admin-default']);
        $display->addScript('ionrangeslider.js', '/packages/ionrangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js', ['admin-default']);


        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $instance = null;
        if($id) {
            $instance = Product::find($id);

            $this->title =  $instance->title;
        }
        
        $panel = AdminForm::panel();
        
        $panel->addBody(
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('title', 'Название')->required(),
                ])
                ->addColumn([
                    AdminFormElement::text('title_en', 'Название англ.'),
                ]),
            AdminFormElement::html('<hr/>'),    
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::text('sku', 'Артикул'),
                    AdminFormElement::text('sku_alternate', 'Альтернативный номер'),
                    AdminFormElement::select('brand_id', 'Производитель')->setModelForOptions('App\Brand'),
                    AdminFormElement::html('<hr/>'),
                    AdminFormElement::text('weight', 'Вес'),
                ])
                ->addColumn([
                    AdminFormElement::text('price', 'Цена')->setDefaultValue(0),
                    AdminFormElement::html('<div class="form-inline form-inline-radio" id="billcurrency">'),    
                    AdminFormElement::radio('currency_id', 'Валюта')->setModelForOptions('App\Currency'),
                    AdminFormElement::html('</div>'),
                    AdminFormElement::html('<hr/>'),
                    AdminFormElement::text('delivery_time', 'Срок поставки'),
                ])
                ->addColumn([
                    AdminFormElement::html('<h4>Изображение</h4>'),
                    AdminFormElement::image('image'),
                ])
        );
        
        
        
        
        // $path = url('/') . '/.../';
        
        // if($instance && $instance->category) $path = url($instance->category->url) . '/';
        
        // $panel->addFooter(
        //     AdminForm::elements([
        //         AdminFormElement::textaddon('slug', 'Адрес на сайте / формируется автоматически от названия')
        //         ->setAddon($path)
        //         ->placeBefore()
        //         ,
        //     ])
        // );
        
        
        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }



    
}
