<?php

namespace App\Http\AdminSections;

use Illuminate\Database\Eloquent\Model;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
//use SleepingOwl\Admin\Display\Column\Editable\AdminColumnEditableText;

use Auth;

use Request;

use App\Bill;
use App\Billitem;
use App\User;
use App\Contractor;
use App\Organization;

class Bills extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;


    /**
     * Initialize class.
     */
    public function initialize()
    {

        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 200);


        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $model->user_id = Auth::id();
        });

        // $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            
        //     $model->updateDeliveryTime();
        // });
        // $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            
        //     $model->updateDeliveryTime();
        // });
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-newspaper-o';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Ком. предложения';
    }


    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display =
            AdminDisplay::datatablesAsync()
                ->setColumns(
                    AdminColumn::link('id', '#')->setWidth('70px'),
                    AdminColumn::link('number', '№'),
                    AdminColumn::datetime('created_at', 'Дата')->setFormat('d.m.Y H:i')->setWidth('140px'),
                    AdminColumn::text('organization.name', 'Организация'),
                    // AdminColumn::text('contractor.title', 'Клиент'),
                    AdminColumn::custom('Клиент', function(Model $model){

                        if(!$model->contractor) return null;

                        return '<a style="white-space:nowrap;" class="label label-default" href="/contractors/'.$model->contractor->id.'/edit">' . $model->contractor->title . '</a>';
                    }),
                    AdminColumn::text('user.name', 'Менеджер')->setWidth('160px'),
                    AdminColumn::text('totalRubFormatted', 'Сумма, руб')->setHtmlAttribute('class', 'text-muted')->setWidth('140px'),
                    AdminColumn::text('totalWeight', 'Общий вес, кг')->setHtmlAttribute('class', 'text-muted')->setWidth('140px'),

                    AdminColumn::custom('Заявка', function(Model $model) {
                        
                        if($model->search_history) {

                            return '<a style="white-space:nowrap;" class="label label-default" href="/search_histories/?id=' . $model->search_history_id . '">' . $model->search_history->title . '</a>';
                        }
                        
                        return '-';
                    })
                )
            ;

        $display->setOrder([[2, 'desc']]);


        $display->setNewEntryButtonText('Создать новое ком. предложение');

        

        $filters = [];

        if(request('user_id') && !empty(request('user_id'))) {

            $user = User::find(request('user_id'));

            $filters[] = AdminDisplayFilter::field('user_id')
                ->setOperator('equal')
                ->setValue(request('user_id'))
                ->setTitle(function($value) use ($user) {
                    return "Менеджер: ". ($user ? $user->name : '#' . request('user_id'));
                });
        }

        if(request('contractor_id') && !empty(request('contractor_id'))) {

            $contractor = Contractor::find(request('contractor_id'));

            $filters[] = AdminDisplayFilter::field('contractor_id')
                ->setOperator('equal')
                ->setValue(request('contractor_id'))
                ->setTitle(function($value) use ($contractor) {
                    return "Контрагент: ". ($contractor ? $contractor->title : '#' . request('contractor_id'));
                });
        }

        if(request('organization_id') && !empty(request('organization_id'))) {

            $organization = Organization::find(request('organization_id'));

            $filters[] = AdminDisplayFilter::field('organization_id')
                ->setOperator('equal')
                ->setValue(request('organization_id'))
                ->setTitle(function($value) use ($organization) {
                    return "Организация: ". ($organization ? $organization->name : '#' . request('organization_id'));
                });
        }

        
        $display->setFilters($filters);


        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id=null)
    {
         $instance = null;
         $bill_actions = [];

        if(!is_null($id)) {

            $instance = Bill::find($id);

            $instance->delivery_time;
        }


        $panel = AdminForm::panel();

        $panel->setHtmlAttribute('id', 'billform');
        
        if($instance) {

            $bill_actions = [
                // AdminFormElement::checkbox('is_draft', 'Черновик')->setDefaultValue(0),

                AdminFormElement::html('<div class="pull-right">'),
                // AdminFormElement::html('<a href="/bills/' . $instance->id . '/print" class="btn btn-sm pull-left" style="margin:0px 0 0 0;text-decoration:line-through;" onclick="return false;"> <i class="fa fa-print"></i> Напечатать</a>'),
                AdminFormElement::html('<a href="/bills/' . $instance->id . '/download?format=xls" class="btn btn-sm pull-left" style="margin:12px 0 0 0;clear:both;"><i class="fa fa-file-excel-o"></i> Скачать (XLS)</a>'),
                // AdminFormElement::html('<a href="/bills/' . $instance->id . '/download?format=pdf" class="btn btn-sm pull-left" style="margin:0px 0 0 0;clear:both;text-decoration:line-through;"><i class="fa fa-file-pdf-o"></i> Скачать (PDF)</a>'),
                AdminFormElement::html('</div>'),

                AdminFormElement::html('<a href="/emails/create?bill_id=' . $instance->id . '" class="btn btn-warning btn-sm pull-right" style="margin:27px 24px 0 0;" data-toggle="tooltip" data-original-title="Создать сообщение эл. почты и прикрепить счёт"><i class="fa fa-envelope-o"></i> Отправить на E-mail</a>'),

            ];
        }


        $panel->addBody([
            AdminFormElement::hidden('id'),
            AdminFormElement::hidden('custom_delivery_time')->setValue(0),

            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text('number', 'Номер предложения'),
                AdminFormElement::select('contractor_id', 'Контрагент')
                    ->setModelForOptions('App\Contractor')->required()
                    ->setHelpText('<a href="/contractors/create?redirect=back" class=""> <i class="fa fa-plus"></i> Создать контрагента</a>'),

            ])
            ->addColumn([
                AdminFormElement::select('organization_id', 'Организация')
                    ->setModelForOptions('App\Organization')
                    ->setDisplay('name'),
            ])
            ->addColumn($bill_actions),
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text('technics', 'Техника'),
                AdminFormElement::text('delivery_time', 'Максимальный срок полной комплектации')
                    ->setHelpText(
                        AdminFormElement::hidden('custom_delivery_time')->setDefaultValue(0) .
                        AdminFormElement::checkbox('custom_delivery_time', 'Указать срок комплектации вручную')->setDefaultValue(($instance && $instance->custom_delivery_time) ? 1 : 0)
                    )
                    ->setReadOnly(($instance && $instance->custom_delivery_time) ? false : true)
            ])
            ->addColumn([
                AdminFormElement::text('paymentmethod', 'Способ оплаты'),
                AdminFormElement::text('delivery_conditions', 'Условия поставки')
            ])
            ->addColumn([
                AdminFormElement::textarea('notes', 'Примечание')->setRows(3)
            ])
        ]);
       


        if($instance) {

            $this->title = 'Коммерческое предложение №' . $instance->id . ' от ' . date('d.m.Y', strtotime($instance->updated_at));




            $items = AdminDisplay::table()
                ->setModelClass(Billitem::class)
                ->setApply(function($query) use($id) {
                    $query->where('bill_id', $id);
                })
                ->setParameter('bill_id', $id)
                ->setColumns(
                    AdminColumn::text('id', '')->setWidth('0px')->setHtmlAttribute('class', '_id width-0'),
                    AdminColumn::text('product.sku', 'Артикул'),
                    AdminColumnEditable::text('title', 'Название'),
                    AdminColumn::text('product.title_en', 'Название англ'),
                    AdminColumnEditable::text('price_formatted', 'Цена продажи'),
                    AdminColumn::text('currency.title', 'Валюта')->setHtmlAttribute('class', 'text-muted'),
                    AdminColumnEditable::text('count', 'Кол-во'),
                    AdminColumn::text('total_rub_formatted', 'Стоимость, руб.')->setWidth('108px')->setHtmlAttribute('class', '__total_rub_formatted'),
                    AdminColumn::text('product.delivery_time', 'Срок поставки'),
                    AdminColumn::text('product.brand.title_country', 'Производитель')->setHtmlAttribute('class', 'text-muted')
                )
                ->setHtmlAttribute('id', 'table_in_form')
                ->setHtmlAttribute('class', 'billitems_in_bill')
                ;

            $panel->addBody([
                $items,
                AdminFormElement::html('<div class="text-center">'),
                AdminFormElement::html('<a href="/?bill_id=' . $instance->id . '" class="btn btn-primary" style="margin:24px 0 0 0" id="btnAddBillProducts"> <i class="fa fa-plus"></i> Добавить позиции</a>'),
                AdminFormElement::html('</div>'),
                AdminFormElement::html('<hr style="margin:48px 0 24px 0;" />'),
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::html('<h4 style="margin-top:0;">Итого в рублях: <span id="total_rub_formatted">' . $instance->totalRubFormatted . '</span> RUB <small> / <span id="with_nds_title">'. $instance->with_nds_title .'</span></small></h4>'),

                        AdminFormElement::html('<div class="form-inline form-inline-radio" id="bill_with_nds" style="margin-bottom:36px;">'),
                        AdminFormElement::radio('with_nds', 'НДС:')
                            ->setOptions([
                                1 => 'Вкл. НДС',
                                0 => 'Без НДС'
                            ])
                            ->setSortable(false)
                            ,
                        AdminFormElement::html('</div>')
                    ])
                    ->addColumn([
                        AdminFormElement::html('<h4 style="margin-top:0;">Итого в валюте КП: <span id="total_cur_formatted">' . $instance->totalCurFormatted . '</span> <span id="currency_title">' . $instance->currency->title . '</span></h4>'),

                        AdminFormElement::html('<div class="form-inline form-inline-radio" id="billcurrency">'),
                        AdminFormElement::radio('currency_id', 'Валюта КП:')->setModelForOptions('App\Currency'),
                        AdminFormElement::html('</div>'),
                    ])
            ]);

        }
        else {

            $panel->addBody([
                AdminFormElement::html('<hr/>'),  
                AdminFormElement::html('<h4 class="text-center">Добавление позиций будет доступно после сохранения КП</h4>')  
            ]);

        }

        

        // $panel->setHtmlAttribute('class', 'form-inline');






        $panel->getButtons()
            ->hideSaveAndCreateButton()

        ;

        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    public function isDeletable(Model $model) {
        return true;
    }
    public function isRestorable(Model $model) {
        return true;
    }
    public function isCreatable() {
        return true;
    }

    public function getCreateTitle()
    {
        return 'Создание коммерческого предложения';
    }
    public function getEditTitle()
    {
        return $this->title;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model) {

        return $model->user_id == Auth::id() || Auth::user()->isSuperAdmin || Auth::user()->isAdmin;
    }


}
