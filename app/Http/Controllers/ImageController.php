<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Image;
use Storage;
use Response;

class ImageController extends Controller
{

    // livetime image in cache in min (default 129600 ~3 month )
    protected $cacheTime = 129600;

    public function __construct()
    {
        $this->middleware('web');
    }

    public function fullImage($dateImg=null, $filename=null)
    {
        $filePath = 'attaches/' . $dateImg .'/'. $filename;

        return Response::make(Storage::get($filePath), 200, array('Content-Type' => 'image/jpeg'));
    }

    /**
     * @param $dateImg
     * @param $filename
     * @param $w
     * @param $h
     * @param null $type
     * @param null $anchor possible: top-left, top, top-right, left, center (default), right, bottom-left, bottom, bottom-right
     * 
     * if($type=='crop') => $anchor -> scale value, $x -> x coord of start cropping, $y -> y coord of start cropping
     * 
     * @return mixed
     */
    public function whResize($dateImg, $filename, $w , $h , $type=null, $anchor=null, $x=null, $y=null)
    {
        $filePath = storage_path('app/attaches/' . $dateImg .'/'. $filename);
        if (! $anchor) $anchor='center';
        if (! $type) $type='outbox';
        if ($w=='w') $w=null;
        if ($h=='h') $h=null;

        $params = (object) array(
            'filePath' =>$filePath,
            'w' => $w,
            'h' => $h,
            'cw' => $w,
            'ch' => $h,
            'anchor' => $anchor,
            'x' => $x,
            'y' => $y
        );

        switch ($type) {
            case 'asis':
                $cacheImage = Image::cache(function($image) use( $filePath, $w, $h, $type){
                    return $image->make($filePath)->resize($w, $h);
                });
                break;
            case 'prop':
                if($w>$h) $params->w = null;
                else $params->h = null;
                $cacheImage = $this->resizeAndChunk($params);
                break;
            case 'chunk':
                if($w>$h) $params->h = null;
                else $params->w = null;
                $cacheImage = $this->resizeAndChunk($params);
                break;
            case 'fit':
                $cacheImage = Image::cache(function($image) use( $params){
                    return $image->make($params->filePath)->fit($params->w, null, null, 'top');
                },$this->cacheTime,false);
                break;
            case 'outbox':
                $cacheImage = Image::cache(function($image) use( $params){
                    return $image->make($params->filePath)->resize($params->w, $params->h, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                },$this->cacheTime,false);
                break;
            case 'crop':
                $cacheImage = Image::cache(function($image) use($params){

                    //retrieving original image to get width & height and scale next
                    $image_original = Image::make($params->filePath);

                    $image = $image
                        ->make($params->filePath)
                        ->resize($image_original->width() * $params->anchor, $image_original->height() * $params->anchor)
                        ->crop($params->w, $params->h, (int)$params->x, (int)$params->y)
                        ->sharpen(0)
                        ->encode('jpg', 90)
                        ;

                    return $image;

                },$this->cacheTime,false);
                break;
        }

        return Response::make($cacheImage, 200, array('Content-Type' => 'image/jpeg'));
    }

    protected function resizeAndChunk($params)
    {
        return Image::cache(function($image) use( $params){
            return $image->make($params->filePath)->resize($params->w, $params->h, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->resizeCanvas($params->cw, $params->ch, $params->anchor);
        },$this->cacheTime,false);
    }

}


/**
 * 
 * $attach->filename — Полное фото;
 * $attach->filename.’/w/350′  —  Высота 350, ширина как получится, пропорционально;
 * $attach->filename.’/350/h’ — Ширина 350, высота как получится, пропорционально;
 * $attach->filename.’/350/150′ — пропорционально отресайзить (то есть высота и ширина не больше указанных, но могут быть меньше),
 * $attach->filename.’/350/150/asis’ — Картинку целиком вписать  ровно по размерам, плевать на пропорции
 * $attach->filename.’/350/150/prop’ — Пропорционально вписать в размеры, при этом подложка ровно по размерам, а сама картинка может быть меньше по одной из сторон
 * $attach->filename.’/350/150/chunk’ — Пропорционально уменьшить до мЕньшей стороны и вырезать кусок оригинала по размеру второй оси, резать по центру.
 * $attach->filename.’/350/150/chunk/anchor’ Тоже, что в предыдущем пункте, только anchor указывает откуда резать (список возможных опций в листинге)
 * 
 * 
 * наложения ватермарков, изменение цвета подложки и прочее => cмотри документацию по Intervention.
 * 
 */ 