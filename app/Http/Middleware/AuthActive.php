<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use URL;

class AuthActive
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);

        // if(Auth::user() && Auth::user()->isActive) {

        //     return $next($request);
        // }

        // if(Auth::user()) {

        //     Auth::logout();
        // }

        // if($request->is('login')) {

        //     return $next($request);
        // }

        // return redirect()->guest('/login')->withErrors(['common' => 'Ваша учётная запись не активна. Обратитесь к администратору.']);
    }
}
