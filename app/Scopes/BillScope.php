<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use DB;

use Auth;

class BillScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if(Auth::guest()) return false;

        if(Auth::user() && Auth::user()->user_status > 0) return $builder;
        
        $table = $model->table ?: str_plural(strtolower(class_basename($model)));
        
        return $builder
            ->whereNull('bills.deleted_at')
            ->where('bills.user_id', Auth::id())
            ;
        ;


        // return $builder
        //     ->whereNull('bills.deleted_at')
        //     ->leftJoin('contractors', 'bills.contractor_id', '=', 'contractors.id')
        //     ->where('contractors.user_id', Auth::id())
        //     ->orWhereNull('bills.contractor_id')
        //     ->select(['bills.id', 'bills.user_id', 'bills.contractor_id', 'bills.is_draft', 'bills.created_at', 'bills.updated_at', 'bills.currency_id'])
        //     ;
        // ;
    }
}