<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Auth;

class UserScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if(Auth::guest()) return false;

        if(Auth::user() && Auth::user()->user_status > 0) return $builder;
        
        $table = $model->table ?: str_plural(strtolower(class_basename($model)));
        
        return $builder
            ->where('user_id', Auth::id());
        ;
    }
}