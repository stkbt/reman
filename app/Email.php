<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Mail;

use App\Scopes\UserScope;
use App\Mail\CommercialProposal;


class Email extends Model
{
	protected $guarded = [];

	protected $casts = [
    	'to' => 'array'
    ];
    
    protected static function boot() {

        self::creating(function($item){

            $item->user_id = Auth::id();
        });
		self::created(function($item){

			$item->sendMessage();
		});

        parent::boot();

        static::addGlobalScope(new UserScope);
    }

    public function bill() {

    	return $this->belongsTo('App\Bill');
    }
    public function user() {

    	return $this->belongsTo('App\User');
    }

    public static function autoTo($bill) {

    	$bills = $bill->contractor->bills()->pluck('id')->all();

    	$tos = Email::whereIn('bill_id', $bills)->pluck('to')->flatten()->unique()->all();


    	return $tos;
    }

    public function generateAttachment() {

        $this->attachment = $this->bill->generateFileExport($this->attachment_type, 'store');
        $this->save();
    }

    public function sendMessage() {

        foreach($this->to as $to) {
            Mail::to($to)->send(new CommercialProposal($this));
        }
    }


    public function getFromAttribute() {

        //bill->user->email

        // if($this->bill && $this->bill->user && $this->bill->user->email)
        //     return $this->bill->user->email;

        if($this->user) return $this->user->email;

        return 'zakaz@reman-service.ru';
    }
}
