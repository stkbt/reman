<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ServiceModels\ImagableTrait;

use App\Scopes\VisibleScope;

use Utils;

class Product extends Model
{
    use ImagableTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku', 'sku_alternate', 'title', 'title_en', 'price', 'currency_id',  'delivery_time_min', 'delivery_time_max', 'weight', 'supplier_id', 'brand_id', 'image', 'priced_at', 'import_id', 'quantity'
    ];

    protected $hidden = ['created_at', 'updated_at', 'hash'];

    protected $hashFields = ['sku', 'brand_id', 'delivery_time_min', 'delivery_time_max'];
    
    protected $appends = ['price_rub', 'price_formatted', 'delivery_time', 'brand_title', 'currency_title'];
    
    
    protected static function boot() {

        self::creating(function($item){

            if($item->sku)
                $item->sku = Utils::cleanSku($item->sku);
            
            if($item->sku_alternate)
                $item->sku_alternate = Utils::cleanSku($item->sku_alternate);

            $item->hash = $item->hashGen;
        });

        parent::boot();

    }
    
    public function brand() {
        
        return $this->belongsTo('App\Brand');
    }
    public function supplier() {
        
        return $this->belongsTo('App\Supplier');
    }
    public function currency() {
        
        return $this->belongsTo('App\Currency');
    }
    

    /* Get Attributes */

    public function getPriceFormattedAttribute() {

        if(isset($this->attributes['price'])) {
            return number_format($this->attributes['price'], 2, '.', ' ');
        }
        else {

            return null;
        }   
    }
    public function getPriceFormattedCurrencyAttribute() {

        if(isset($this->attributes['price'])) {

            $val = number_format($this->attributes['price'], 2, '.', ' ');

            if($this->currency) {
                $val .= ' ' . $this->currency->title;
            }

            return $val;
        }
        else {

            return null;
        }   
    }


    public function getPriceRubAttribute() {

        if(isset($this->attributes['price'])) {
            
            if($this->currency_id == 1) return number_format($this->attributes['price'], 2, '.', ' ');

            return number_format($this->attributes['price']*$this->currency->rate, 2, '.', ' ');
        }
        else {

            return null;
        }

        
    }
    public function getPriceRubValueAttribute() {

        if($this->currency_id == 1) return $this->attributes['price'];

        return $this->attributes['price']*$this->currency->rate;
    }


    public function getDeliveryTimeAttribute() {

        if(isset($this->attributes['delivery_time_min']))
            $min = $this->attributes['delivery_time_min'];
        
        if(isset($this->attributes['delivery_time_max']))
            $max = $this->attributes['delivery_time_max'];

        if(isset($min) && isset($max))
            return Utils::localizePeriod($min, $max);


        return '–';
    }
    

    public function getBrandTitleAttribute() {

        if($this->brand) {
            return $this->brand->title;
        }

        return null;
    }
    public function getCurrencyTitleAttribute() {

        if($this->currency) {
            return $this->currency->title;
        }

        return null;
    }


    public function getQuantityAttribute() {

        if(array_get($this->attributes, 'delivery_time_min') == 0 && array_get($this->attributes, 'delivery_time_max') == 0) {
            return isset($this->attributes['quantity']) ? $this->attributes['quantity'] : null;
        }

        return '–';
    }


    public function getCountryIdAttribute() {

        if($this->brand) {
            return $this->brand->country_id;
        }

        return null;
    }
    
    /* Set Attributes */
    
    public function setImageAttribute($image) {
        
        $this->setImagable('image', $image);
    }

    public function setDeliveryTimeAttribute($value) {

        $values = Utils::parsePeriod($value);

        $this->attributes['delivery_time_min'] = array_get($values, 'min');
        $this->attributes['delivery_time_max'] = array_get($values, 'max');
    }
    
    public function setPriceFormattedAttribute($value) {

        $value = str_replace(' ', '', $value);

        $this->attributes['price'] = $value;
    }

    /* Utils */

    // public function createHash() {


    //     $this->hash = $this->hashGen;

    //     return $this;
    // }

    public function getHashGenAttribute() {

        return md5(collect($this->attributes)->only($this->hashFields)->implode(''));
    }

}
