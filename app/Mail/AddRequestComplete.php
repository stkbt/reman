<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use App\Email;
use App\Bill;

class AddRequestComplete extends Mailable
{
    use Queueable, SerializesModels;

    public $addrequest = null;
    public $attachment = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\AddRequest $addrequest)
    {
        $this->addrequest = $addrequest;


        $this->attachment = array_get($this->addrequest->search_history->generateFileExport('xls', 'store'), 'full', null);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mime = [
            'as' => $this->addrequest->search_history->title . '.xls',
            'mime' => 'application/xls',
        ];

        return $this
            ->from($this->addrequest->handler->email)
            ->view('mail.addrequestcomplete', ['addrequest' => $this->addrequest])
            ->subject('Запрос на добавление позиций обработан')
            ->attach($this->attachment, $mime)
        ;
    }
}
