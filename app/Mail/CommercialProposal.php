<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use App\Email;
use App\Bill;

class CommercialProposal extends Mailable
{
    use Queueable, SerializesModels;

    public $email = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;

        $this->email->generateAttachment();

        $this->email->sended_at = Carbon::now();
        $this->email->save();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mime = null;
        if($this->email->attachment) {

            switch ($this->email->attachment_type) {
                case 'pdf':
                    $mime = [
                            'as' => $this->email->bill->filename . '.pdf',
                            'mime' => 'application/pdf',
                        ];
                    break;
                
                case 'xls':
                    $mime = [
                            'as' => $this->email->bill->filename . '.xls',
                            'mime' => 'application/xls',
                        ];
                    break;
            }
        }

        return $this
            ->from($this->email->from)
            ->view('mail.cp')
            ->subject($this->email->theme)
            ->attach($this->email->attachment ? storage_path($this->email->attachment) : null, $mime)
        ;
    }
}
