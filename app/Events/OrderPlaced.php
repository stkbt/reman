<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Order;

class OrderPlaced extends Event
{
    use SerializesModels;
    
    public $order = null;
    public $user   = null;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $_order, $_user = null)
    {
        
        $this->order = $_order;
        $this->user = $_user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
