<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Excel;

use App\Scopes\UserScope;

class SearchHistory extends Model
{
    protected $guarded = [];

    protected $casts = [
    	'sku' => 'array',
    	'brand_id' => 'array',
        'result' => 'array'
    ];


    protected static function boot() {

		self::creating(function($item){

            $user = Auth::user();

			$item->user_id = $user->id;

            if(!$item->title)
                $item->title = self::generateTitle();
		});

        parent::boot();

        static::addGlobalScope(new UserScope);
    }

    public function user() {

        return $this->belongsTo('App\User');
    }

    public function contractor() {

        return $this->belongsTo('App\Contractor');
    }

    public function bills() {

    	return $this->hasMany('App\Bill');
    }


    public function setSkuRawAttribute($value) {
        
        $this->attributes['sku'] = $value;
    }


    public static function generateTitle() {

        $user = Auth::user();

        $title = $user->nickname . ' ' . date('dm');

        $user_today_count = self::userToday($user->id)->count();

        return $title . '-' . ($user_today_count+1);
    }



    /* Scopes */

    public function scopeUserToday($query, $user_id) {

        return $query->where('user_id', $user_id)->whereDate('created_at', date('Y-m-d'));
    }



    public function generateFileExport($format = 'xls', $action = 'download') {


        $template = storage_path('templates/kp.xls');
        $signtext = '';


        // if($this->organization) {

        //     if($this->organization->form_file) {

        //         $form_file = public_path($this->organization->form_file);

        //         if(is_file($form_file)) {

        //             $template = $form_file;
        //         }
        //     }

        //     $signtext = $this->organization->director;
        // }


        $export = Excel::load($template, function($reader) {

            $reader->noHeading();

        });

        $sheet = $export->getSheet();

        $sheet->setCellValue('B2', 'Номер заявки');
        $sheet->setCellValue('C2', $this->title);
        $sheet->setCellValue('C3', $this->contractor ? $this->contractor->title : '');
        $sheet->setCellValue('C4', '');
        $sheet->setCellValue('C6', $this->user->name);
        $sheet->setCellValue('C7', $this->user->phone);
        
        $sheet->setCellValue('C8', $this->user->email);
        $sheet->getCell('C8')->getHyperlink()->setUrl('<a href="mailto:'.$this->user->email.'">' . $this->user->email . '</a>');
        
        
        $sheet->setCellValue('C9', 'Заявка');
        $sheet->setCellValue('C10', '');
        $sheet->setCellValue('C11', '');
        $sheet->setCellValue('C12', '');
        $sheet->setCellValue('C13', '');


        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
            'font' => [
                'size' => 10
            ]
        ];


        $data = [];

        $num = 0;
        $row_start = 16;



        $skus = collect($this->sku);
        $brand_id = $this->brand_id;
        $search_alternate = $this->search_alternate;

        // $products = \App\Admin\Controllers\SearchController::getProducts($skus->keys()->all(), $brand_id, $search_alternate)->keyBy('id');


        $result = collect($this->result)->keyBy('sku');

        $items = collect($this->sku);

        foreach ($items as $sku => $value) {
            
            $num++;
            
            // $product = $products->get($item['id']);
            
            $item = $result->get($value['sku']);


            if(!$item) {

                $data[] = [
                    $num,
                    '–',
                    array_get($value, 'sku'),
                    '–',
                    array_get($value, 'count'),
                    '–',
                    '–',
                    '–',
                    '–'
                ];

                continue;
            }

            $data[] = [
                $num,
                array_get($item, 'title'),
                array_get($item, 'sku'),
                array_get($item, 'sku_alternate'),
                array_get($value, 'count'),
                array_get($item, 'price') . array_get($item, 'currency_title'),
                number_format((float)array_get($item, 'price', 0) * array_get($item, 'count', 1), 2, ',', ' '),
                array_get($item, 'brand_title'),
                array_get($item, 'delivery_time')
            ];

            
        }

        $sheet->fromArray($data, null, 'A' . $row_start, true);

        $sheet
            ->getStyle('A16:I' . ($row_start + $num - 1))
            ->applyFromArray($style)
            ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
            ;


        $sheet
            ->setCellValue('G' . ($row_start + $num), $this->total)
            ->getStyle('G' . ($row_start + $num))
            ->applyFromArray($style)
            ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
            ;

        $sheet
            ->setCellValue('F14', 'Цена')
            ->setCellValue('G14', 'Сумма');


        $signcoord = [$row_start + $num + 6, $row_start + $num + 7];




        $sheet
            ->mergeCells('H' . $signcoord[0] . ':I' . $signcoord[1])
            ->setCellValue('H' . $signcoord[0], $signtext . "\n")
            ->getStyle('H' . $signcoord[0])
            ->applyFromArray([
                'alignment' => [
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ],
                'font' => [
                    'size' => 10,
                    'bold' => true
                ]
            ])
            ->getAlignment()->setWrapText(true)
            ;


        /* fix wtf red border */
        $k = 1;
        while($k < 14) {
            $sheet
                ->getStyle('A' . $k)
                ->getBorders()
                ->getLeft()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }
        $k = 1;
        while($k < 14) {
            $sheet
                ->getStyle('I' . $k)
                ->getBorders()
                ->getRight()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }
        $k = 0;
        while($k < 9) {
            $sheet
                ->getStyleByColumnAndRow($k, 1)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN)
                ->getColor()
                ->setRGB('BBBBBB')
                ;

            $k++;
        }

        
        if($action == 'download') {
            $export->setFileName($this->title)->export($format);
        }
        elseif($action == 'store') {

            return $export->setFileName($this->title)->store('xls', false, true);
        }
    }
}
