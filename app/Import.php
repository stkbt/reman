<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
use Excel;
use Carbon\Carbon;
use Utils;

class Import extends Model
{
	protected $fillable = [
        'filename', 'user_id', 'rows_total', 'row_last', 'is_complete'
    ];


    protected static function boot() {

		self::creating(function($item){

			$item->user_id = Auth::id();
		});

        parent::boot();

    }
    
    public function user() {

    	return $this->belongsTo('App\User');
    }

    public function import_products() {

        return $this->hasMany('App\ImportProduct');
    }


    public function getHasProductsAttribute() {

        return $this->import_products->where('is_imported', 0)->count() > 0;
    }


    /* Import operation */

    protected $currencies = [
        'RUR' => 1,
        'USD' => 2,
        'EUR' => 3,
    ];

    protected $brands = null;
    protected $countries = null;

    

    public function runImport($take = null) {

        $filename = public_path($this->filename);

        $data = Excel::load($filename, function($reader) {})->all()->get(0);

        $row_last = 0;

        if($this->row_last) {

            $data = $data->slice($this->row_last-1);

            $row_last = $this->row_last;
        }

        if($take) {

            $data = $data->take($take);
        }


        foreach($data as $row) {

            $values = $row->values();

            $delivery_range = Utils::parsePeriod($values[8]);

            $country = isset($values[10]) ? $values[10] : null;

            $product = ImportProduct::create([
                'sku' => (string)$values[0],
                'quantity' => $values[1],
                'sku_alternate' => (string)$values[2],
                'title' => $values[3],
                'title_en' => $values[4],
                'weight' => $values[5],
                'price' => $values[6],
                'currency_id' => $this->getCurrency($values[7]),
                'delivery_time_min' => $delivery_range['min'],
                'delivery_time_max' => $delivery_range['max'],
                'brand_id' => $this->getBrand($values[9], $country),
                'supplier_id' => 1,
                'import_id' => $this->id
            ]);

            if($product) $row_last++;
        }

        if(!$this->rows_total) $this->rows_total = $data->count();
        $this->row_last = $row_last;

        if($this->rows_total == $this->row_last) $this->is_complete = 1;

        $this->save();

        // $this->setImportProductsIsDuplicate();

        return true;
    }



    private function getCurrency($_symbol) {

        if(isset($this->currencies[$_symbol])) return $this->currencies[$_symbol];

        return 1;
    }

    private function getBrand($_title, $_country_title = null) {

        if(!$this->brands) $this->brands = Brand::all();
        if(!$this->countries) $this->countries = Country::all();

        $country_id = null;
        $new_country_id = null;
        if(!is_null($_country_title)) {
            $country = $this->countries->where('title', $_country_title);

            if($country->count()) {
                $country = $country->first();
                $country_id = $country->id;
            }
            else {

                // $country = Country::create([
                //     'title' => $_country_title
                // ]);
                // $country_id = $country->id;
            }
        }

        $brand = $this->brands->where('title', $_title)->where('country_id', $country_id);

        if($brand->count() == 1) {

            return $brand->first()->id;
        }
        elseif($brand->count() > 1) {

            foreach ($brand as $brand_item) {
                if($brand_item->country_id == $country_id) {

                    return $brand_item->id;
                }
            }

            return $brand->first()->id;

        }
        else {

            $brand = $this->brands->where('title', $_title);

            if(!$brand->count()) {

                // $brand = Brand::create([
                //     'title' => $_title,
                //     'country_id' => $country_id
                // ]);

                // $this->brands = Brand::all();

                // return $brand->id;
            }
            else {

                $brand = $brand->first();

                return $brand->id;
            }

            
        }


        // if(isset($this->brands[$_title])) return $this->brands[$_title];

        

        return null;
    }

    // public function setFilenameAttribute($filename) {

    //     if(is_file(public_path($filename))) {

    //         $info = pathinfo($filename);

    //         $path = 'imports/'.date ('Y-m-d').'/';

    //         $newFileName = md5(date('Y-m-d-s').$filename).'.'.$info['extension'];

    //         Storage::put(
    //             $path.$newFileName,
    //             file_get_contents(public_path($filename))
    //         );

    //         @unlink(public_path($filename));

    //         $this->attributes[$field] = $path.$newFileName;
    //     }
    //     else {
    //         $this->attributes[$field] = null;
    //     }
    // }

}
